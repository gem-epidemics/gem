GEM Developer Documentation
===============================================================================

GEM is a domain-specific modelling language for epidemics, providing a bridge
between applied epidemic modelling and computational and statistical
methodology. The package is written in Python, and is based conceptually around
4 layers:

1. The GEM model description language, *gemlang*;
2. Maths functions encoding the probabilistic GEM model;
3. A back-end extensible algorithms layer for simulation and inference;
4. A scalable and hardware-independent maths layer, currently `Tensorflow
   <https://www.tensorflow.org>`_

GEM provides both a user interface and a developer interface, allowing use by
epidemics scientists analysing outbreaks as well as methods researchers
(statisticians, computer scientists, etc) to add new functionality to the
package. Essentially, GEM is a source-to-source translator, taking a
**gemlang** input specification and translating it to a Python representation.
All translation and launching of a given model is conducted within a parent
language runtime interface, initially Python. Future plans are in place to
develop a thin R wrapper around the Python interface.

This guide is intended to allow developers to understand the GEM architecture
enough to be able to extend, maintain, and develop the package.

.. toctree::
    :maxdepth: 1
    :caption: GEM software components

    gemlang
    interface
