GEM documentation
===============================================================================

Welcome to the GEM documentation. This is your one-stop shop for all things
GEM-related.

.. toctree::
   :maxdepth: 1
   :caption: Parts of the documentation:

   userguide/index
   developer/index
   gem


Indices and tables
===============================================================================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
