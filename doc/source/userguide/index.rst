GEM User Guide
===============================================================================

GEM is a toolkit for epidemic analysis. It is a probabilistic programming
language that allows users to define an epidemic process in a clear, repeatable
language, and embed that process into a higher-order probabilistic model. It
provides for both simulation and inference processes.

To install GEM::

  $ pip install git+http://fhm-chicas-code.lancs.ac.uk/GEM/gem.git


Example usage::

  from gem import GEM
  from gem.plotting import plot_state_timeseries, traceplot

  prog = """
  beta ~ Gamma(2, 10)
  gamma ~ Gamma(1, 10)

  Epidemic MyEpidemic() {

      S = State(init=999)
      I = State(init=1)
      R = State(init=0)

      [S -> I] = beta * I / 1000
      [I -> R] = gamma
  }

  epi ~ MyEpidemic() """

  # Compile model = GEM(prog)

  # Simulate
  sim = model.sample(
      n_samples=1, condition_vars={'beta': 0.4, 'gamma': 0.14}, seed=3
  )
  plot_state_timeseries(model.['epi'], sim.epi, state_labels=("S", "I", "R"))

  # Inference
  posterior, accept = model.fit(
      observed={'epi': sim.epi},
      num_samples=10000,
      init={'beta': 0.1, 'gamma': 0.1},
      num_burnin_samples=2000
  )
  traceplot(posterior)


Status
-------------------------------------------------------------------------------

GEM is currently very much in alpha testing, with the interface likely to
change without warning. So far, the software is able to perform inference on
epidemic models with known transition times. It can also perform inference on a
range of non-epidemic probabilistic models.

Watch this space in the coming year for developments in GEM, specifically
around partially-observed epidemic processes and hidden-Markov models.

Get involved
-------------------------------------------------------------------------------

If you like the idea of GEM, get involved! Clone the Git repo, and/or contact
the project leader, Chris Jewell <c.jewell@lancaster.ac.uk>.

Acknowledgements
-------------------------------------------------------------------------------

I'd very much like to thank the [University of
Lancaster](http://www.lancaster.ac.uk) for providing me a base from which to
develop GEM, [CHICAS](http://chicas.lancaster-university.uk/) my group at
Lancaster, and most of all [The Wellcome Trust](http://www.wellcome.ac.uk) for
providing funding
[funding](https://wellcome.ac.uk/funding/people-and-projects/grants-awarded/gem-translational-software-outbreak-analysis)
to take GEM to the next level.
