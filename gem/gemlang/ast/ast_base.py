##############################################################################
# Copyright 2019 Chris Jewell <c.jewell@lancaster.ac.uk>                     #
#                                                                            #
# Permission is hereby granted, free of charge, to any person obtaining a    #
# copy of this software and associated documentation files (the "Software"), #
# to deal in the Software without restriction, including without limitation  #
# the rights to use, copy, modify, merge, publish, distribute, sublicense,   #
# and/or sell copies of the Software, and to permit persons to whom the      #
# Software is furnished to do so, subject to the following conditions:       #
#                                                                            #
# The above copyright notice and this permission notice shall be included    #
# in all copies or substantial portions of the Software.                     #
#                                                                            #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR #
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   #
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    #
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER #
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    #
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        #
# DEALINGS IN THE SOFTWARE.                                                  #
##############################################################################

"""Base classes for GEM Abstract Syntax Tree"""

__all__ = [
    "GEMProgram",
    "Statement",
    "ASTNode",
    "NullNode",
    "Block",
    "FArg",
]  # Augmented automatically with operator classes


# Generic expression framework
class ASTNode:
    """
    Generic abstract syntax tree node

    :param meta: line/col metainformation
    """

    def __init__(self, meta=None):
        self.__children__ = []
        self.__meta__ = meta
        # self.symbol = None
        self.scope = None

    @property
    def children(self):
        """:return a list of child nodes."""
        return self.__children__

    @children.setter
    def children(self, children):
        """:return a list of child nodes."""
        self.__children__ = children

    @property
    def meta(self):
        """:return node metadata."""
        return self.__meta__

    def add_child(self, child):
        """
        Adds a child node to the AST

        :param child a child node of type ASTNode (or derived)
        """
        if not isinstance(child, ASTNode):
            raise AssertionError
        self.__children__.append(child)

    def has_children(self):
        """:return True if the node has children, else False"""
        return len(self.children) > 0

    def __str__(self):
        return self.__class__.__name__

    def __repr__(self):
        return self.__class__.__name__


class NullNode(ASTNode):
    """
    Represents NULL node. This can be used to effectively remove subtrees from
    the AST.

    :param value: a string used to describe the NullNode.
    """

    def __init__(self, value, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.value = value

    def __repr__(self):
        return f"NullNode(<{self.value}>)"

    def __str__(self):
        return str(self.value)


class GEMProgram(ASTNode):
    """
    Represents a GEM program

    :param statements: list of objects of type Statement
    """

    def __init__(self, statements, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for stmt in statements:
            assert isinstance(stmt, Statement)
            self.add_child(stmt)


class Block(GEMProgram):
    """
    Represents a code block

    :param statements: list of objects of type Statement
    """

    def __init__(self, statements, **kwargs):
        statements = [s for s in statements if isinstance(s, ASTNode)]
        super().__init__(statements, **kwargs)


class Statement(ASTNode):
    """Statement base class.  This is not meant to be used
    as part of the AST API."""

    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class IndexList(ASTNode):
    """Represents an index list for an indexed compound structure."""

    def __init__(self, index_list, **kwargs):
        super().__init__(**kwargs)
        for index in index_list:
            self.add_child(index)


class FArg(ASTNode):
    """
    Represents a function argument definition. :param name: the argument name
    """

    def __init__(self, name, **kwargs):
        super().__init__(**kwargs)
        self.value = name

    def __repr__(self):
        return f"{self.__class__.__name__} {self.value}"

    def __str__(self):
        return str(self.value)


class FArgList(ASTNode):
    """
    Represents a list of ArgDefs :param args: a list of ArgDefs
    """

    def __init__(self, argdefs, **kwargs):
        super().__init__(**kwargs)
        for arg in argdefs:
            assert isinstance(arg, FArg)
            self.add_child(arg)
