##############################################################################
# Copyright 2019 Chris Jewell <c.jewell@lancaster.ac.uk>                     #
#                                                                            #
# Permission is hereby granted, free of charge, to any person obtaining a    #
# copy of this software and associated documentation files (the "Software"), #
# to deal in the Software without restriction, including without limitation  #
# the rights to use, copy, modify, merge, publish, distribute, sublicense,   #
# and/or sell copies of the Software, and to permit persons to whom the      #
# Software is furnished to do so, subject to the following conditions:       #
#                                                                            #
# The above copyright notice and this permission notice shall be included    #
# in all copies or substantial portions of the Software.                     #
#                                                                            #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR #
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   #
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    #
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER #
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    #
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        #
# DEALINGS IN THE SOFTWARE.                                                  #
##############################################################################

"""AST Expression classes"""

__all__ = [
    "Expr",
    "UnaryExpr",
    "BinaryExpr",
    "IndexExpr",
    "Call",
    "IdRef",
    "Array",
    "Number",
    "Integer",
    "Float",
    "String",
    "TransitionRef",
    "ArgList",
    "KwRef",
    "KwArg",
]

import operator

from gem.gemlang.ast.ast_base import Statement
from gem.gemlang.semantics.scope import Scope

_BINARY_OPS = [
    "truediv",
    "mul",
    "matmul",
    "add",
    "sub",
    "pow",
    "lt",
    "gt",
    "le",
    "ge",
    "eq",
    "ne",
]
_UNARY_OPS = ["neg", "not_"]

_OP_SYMBOLS = {
    "truediv": "/",
    "mul": "*",
    "matmul": "@",
    "add": "+",
    "sub": "-",
    "pow": "^",
    "lt": "<",
    "gt": ">",
    "le": "<=",
    "ge": ">=",
    "ne": "!=",
    "neg": "-",
    "not_": "!",
}


class Expr(Statement):
    """
    Expression base class

    :class:`Expr` derived classes represent expressions in :mod:`gem.gemlang`.
    :class:`Expr` is not intended to be used itself.
    """

    def __init__(self, *args, **kwargs):
        self.__eval_type = None
        super().__init__(*args, **kwargs)

    @property
    def eval_type(self):
        return self.__eval_type

    @eval_type.setter
    def eval_type(self, type_symbol):
        self.__eval_type = type_symbol

    def __str__(self):
        return str(self.__class__).rsplit(".", maxsplit=1)[-1][:-2]


class UnaryExpr(Expr):
    """
    Represents a unary expression.

    :class:`UnaryExpr` is not intended to be used directly, but may be
    specialised into specific unary ops, e.g. log, exp, not, neg, etc.
    """

    def __init__(self, x, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.add_child(x)


for opname in _UNARY_OPS:
    # pylint: disable=unnecessary-lambda
    TYPENAME = opname.capitalize() + "Expr"
    globals()[TYPENAME] = type(
        TYPENAME,
        (UnaryExpr,),
        {
            "__init__": lambda self, *args, **kwargs: UnaryExpr.__init__(
                self, *args, **kwargs
            ),
            "op": operator.__dict__[opname],
        },
    )
    __all__.append(TYPENAME)


class IndexExpr(UnaryExpr):
    def __init__(self, expr, indices, *args, **kwargs):
        super().__init__(expr, **kwargs)
        self.add_child(indices)


class BinaryExpr(Expr):
    """
    A binary expression.

    :class:`BinaryExpr` is not intended to be used directly, but may be
    specialised into specific binary operators, e.g. '+', '-", etc.
    """

    def __init__(self, lhs, rhs, **kwargs):
        super().__init__(**kwargs)
        self.add_child(lhs)
        self.add_child(rhs)


# Dynamically generate operator classes
for opname in _BINARY_OPS:
    # pylint: disable=unnecessary-lambda
    TYPENAME = opname.capitalize() + "Expr"
    globals()[TYPENAME] = type(
        TYPENAME,
        (BinaryExpr,),
        {
            "__init__": lambda self, *args, **kwargs: BinaryExpr.__init__(
                self, *args, **kwargs
            ),
            "op": operator.__dict__[opname],
        },
    )
    __all__.append(TYPENAME)


class Call(Expr):
    """
    A function call.

    :param fname: the function name
    :param fargs: an :class:`ArgList` of arguments.

    :ivar children: a list of `[fname, fargs]`
    """

    def __init__(self, fname, fargs, *args, **kwargs):
        super().__init__(*args, **kwargs)
        assert isinstance(fname, IdRef)
        assert isinstance(fargs, ArgList)
        self.add_child(fname)
        self.add_child(fargs)


class IdRef(Expr):
    """
    Reference to a symbol.

    :param name: the name of the symbol.
    """

    def __init__(self, name, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.value = str(name)  # :ivar the symbol name (str)
        self.symbol = None

    def __repr__(self):
        return f"{self.__class__.__name__} {self.value}"

    def __str__(self):
        return self.__repr__()


# Data literals
class Array(Expr):
    """
    Represents an n-dimensional array literal.

    :param elements: a list of array elements
    """

    def __init__(self, elements, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for element in elements:
            self.add_child(element)

    def __str__(self):
        str_children = [str(child) for child in self.children]
        return f"[{', '.join(str_children)}]"

    def __repr__(self):
        repr_children = [repr(child) for child in self.children]
        return f"{self.__class__.__name__} '{repr_children}'"


class Number(Expr):
    """
    Represents a number

    :param data: a number
    """

    def __init__(self, data, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.value = data  # :ivar the value of the number.

    def __str__(self):
        return str(self.value)

    def __repr__(self):
        return f"{self.__class__.__name__} {self.value}"


class Integer(Number):
    """
    Represents an integer :param data: an integer
    """

    def __init__(self, data, *args, **kwargs):
        data = int(data)
        super().__init__(data, *args, **kwargs)


class Float(Number):
    """
    Represents a float :param data: a floating point number
    """

    def __init__(self, data, *args, **kwargs):
        data = float(data)
        super().__init__(data, *args, **kwargs)


class String(Expr):
    """
    Represents a string.

    :param data: the string.
    """

    def __init__(self, data, **kwargs):
        super().__init__(**kwargs)
        self.value = data  #: value: the value of the string.

    def __repr__(self):
        return 'String "{}"'.format(self.value)

    def __str__(self):
        return self.value


class TransitionRef(Expr):
    """
    Denotes a transition definition.

    Transitions represent paths between States.

    :param src: the source State
    :param dest: the destination State

    :ivar children: a list of ASTNodes `[src, dest]`
    """

    def __init__(self, src, dest, *args, **kwargs):
        super().__init__(*args, **kwargs)
        assert isinstance(src, IdRef)
        assert isinstance(dest, IdRef)
        self.add_child(src)
        self.add_child(dest)


class ArgList(Statement):
    """
    An argument list.

    :param args: a list of arguments
    :type args: :class:`list` of :class:`Expr` and/or :class:`KwArg`

    :ivar children:
    """

    def __init__(self, args, **kwargs):
        super().__init__(**kwargs)
        for arg in args:
            assert isinstance(
                arg, (Expr, KwArg)
            ), f"arg is not of type (Expr, KwArg), but is instead {type(arg)}"
            self.add_child(arg)
        self.__kwarg_scope = None

    @property
    def kwarg_scope(self):
        """:return the keyword scope"""
        return self.__kwarg_scope

    @kwarg_scope.setter
    def kwarg_scope(self, scope):
        assert isinstance(scope, Scope)
        self.__kwarg_scope = scope
        # pylint: disable=expression-not-assigned
        [
            setattr(arg, "kwarg_scope", scope)
            for arg in self.children
            if isinstance(arg, KwArg)
        ]

    def __len__(self):
        return len(self.children)


class KwArg(Statement):
    """
    Represents a keyword argument.

    Keyword arguments bind arguments to specific parameters, e.g. foo(bar=baz).

    :param symbol: the name of the keyword parameter (e.g. bar)
    :param expr: the expression to assign to the keyword parameter.

    :attr children: a list of AST nodes `[symbol, expr]`.
    """

    def __init__(self, symbol, expr, **kwargs):
        super().__init__(**kwargs)
        self.__kwarg_scope = None
        assert isinstance(symbol, KwRef)
        assert isinstance(expr, Expr)
        self.add_child(symbol)
        self.add_child(expr)

    @property
    def kwarg_scope(self):
        """:return the keyword scope"""
        return self.__kwarg_scope

    @kwarg_scope.setter
    def kwarg_scope(self, scope):
        """
        Sets a keyword argument scope

        :param scope: the scope to set
        """
        assert isinstance(scope, Scope)
        self.__kwarg_scope = scope


class KwRef(Expr):
    """
    Represents a reference to a keyword in function arguments.

    :param name: keyword name
    """

    def __init__(self, name, **kwargs):
        super().__init__(**kwargs)
        self.value = name

    def __repr__(self):
        return "{cls} {name}".format(
            cls=self.__class__.__name__, name=self.value
        )

    def __str__(self):
        return str(self.value)
