##############################################################################
# Copyright 2019 Chris Jewell <c.jewell@lancaster.ac.uk>                     #
#                                                                            #
# Permission is hereby granted, free of charge, to any person obtaining a    #
# copy of this software and associated documentation files (the "Software"), #
# to deal in the Software without restriction, including without limitation  #
# the rights to use, copy, modify, merge, publish, distribute, sublicense,   #
# and/or sell copies of the Software, and to permit persons to whom the      #
# Software is furnished to do so, subject to the following conditions:       #
#                                                                            #
# The above copyright notice and this permission notice shall be included    #
# in all copies or substantial portions of the Software.                     #
#                                                                            #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR #
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   #
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    #
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER #
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    #
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        #
# DEALINGS IN THE SOFTWARE.                                                  #
##############################################################################

"""Statement-related ASTNode derived classes defined here."""

from gem.gemlang.ast.ast_base import Block, FArgList, Statement
from gem.gemlang.ast.ast_expression import ArgList, Expr, IdRef, TransitionRef

__all__ = [
    "Decl",
    "EpiDecl",
    "AssignExpr",
    "AssignTransition",
    "StochasticAssignExpr",
]


class Decl(Statement):
    """Declaration base class."""

    def __str__(self):
        return "{type}".format(type=self.__class__.__name__)


class EpiDecl(Decl):
    """
    Declares an epidemic.

    :param name: the name of the epidemic process model
    :param config: an :class:`ArgList` of configuration parameters
                   (type,time_origin,time_step)
    :param param: arguments supplied to the epidemic, useful for scoping
                  purposes
    :param block: a :class:`Block` of :class:`Statements`

    :ivar children: a list of `[name::Id, config::ArgList, param::ArgList,
                    block::Block]`
    """

    def __init__(self, name, config, param, block, **kwargs):
        super().__init__(**kwargs)
        assert isinstance(name, str)
        assert isinstance(config, ArgList)
        assert isinstance(
            param, FArgList
        ), f"param of type {type(param)} is not ArgDefList"
        assert isinstance(block, Block)
        self.name = name
        self.add_child(config)
        self.add_child(param)
        self.add_child(block)

    def __repr__(self):
        return f"EpiDecl {self.name}"

    __str__ = __repr__


class AssignExpr(Statement):
    """
    Assignment of lhs = rhs

    :param symbol: the symbol representing the lhs of the assignment
    :param expr: an expression on the rhs of the assignment

    :ivar children: a list of `[symbol::(IdRef, Decl, TransitionDef),
                    expr::Expr]`
    """

    def __init__(self, symbol, expr, *args, **kwargs):
        super().__init__(*args, **kwargs)
        assert isinstance(symbol, IdRef)
        assert isinstance(expr, Expr)
        self.add_child(symbol)
        self.add_child(expr)


class StochasticAssignExpr(Statement):
    """
    Stochastic assignment lhs ~ rhs

    :param param: parameter to assign
    :param expr: expression to assign

    :ivar children: list of `[param::IdRef, expr::Expr]`
    """

    def __init__(self, param, expr, *args, **kwargs):
        super().__init__(*args, **kwargs)
        assert isinstance(param, IdRef)
        assert isinstance(expr, Expr)
        self.add_child(param)
        self.add_child(expr)


class AssignTransition(Statement):
    """
    Assignment of a Transition rate [A -> B] = expr

    :param transition: a transition
    :param expr: expression to assign
    """

    def __init__(self, transition, expr, *args, **kwargs):
        super().__init__(*args, **kwargs)
        assert isinstance(transition, TransitionRef)
        assert isinstance(expr, Expr)
        self.add_child(transition)
        self.add_child(expr)
