##############################################################################
# Copyright 2021 Chris Jewell <c.jewell@lancaster.ac.uk>                     #
#                                                                            #
# Permission is hereby granted, free of charge, to any person obtaining a    #
# copy of this software and associated documentation files (the "Software"), #
# to deal in the Software without restriction, including without limitation  #
# the rights to use, copy, modify, merge, publish, distribute, sublicense,   #
# and/or sell copies of the Software, and to permit persons to whom the      #
# Software is furnished to do so, subject to the following conditions:       #
#                                                                            #
# The above copyright notice and this permission notice shall be included    #
# in all copies or substantial portions of the Software.                     #
#                                                                            #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR #
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   #
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    #
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER #
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    #
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        #
# DEALINGS IN THE SOFTWARE.                                                  #
##############################################################################

"""Generates a UML-style representation of an AST"""

from uuid import uuid4 as uuid

import graphviz as gv

from gem.gemlang.ast.ast_expression import Expr, IdRef, KwRef, Number


def _build_graph(node, graph):

    node_id = str(uuid())
    node_scope = node.scope.name if node.scope else None

    label = f"{{{node.__class__.__name__} |"

    if isinstance(node, (IdRef, Number)):
        label += f"+ value: {node.value}\\l"

    if isinstance(node, (IdRef, KwRef)):
        label += f"+ symbol: {node.symbol.__class__.__name__}\\l"
        label += f"+ scope: {node_scope}\\l"

    if isinstance(node, Expr):
        if node.eval_type is not None:
            label += f"+ eval_type: {node.eval_type}\\l"
        else:
            label += "+ eval_type: None\\l"
    label += "}}"
    graph.node(
        node_id,
        label=label,
    )

    for child in node.children:
        child_id = _build_graph(child, graph)
        graph.edge(node_id, child_id)

    return node_id


def ast2dot(ast):
    """Takes an AST and draws UML"""

    dot = gv.Digraph("AST", node_attr={"shape": "record"})

    _build_graph(ast, dot)

    return dot
