##############################################################################
# Copyright 2019 Chris Jewell <c.jewell@lancaster.ac.uk>                     #
#                                                                            #
# Permission is hereby granted, free of charge, to any person obtaining a    #
# copy of this software and associated documentation files (the "Software"), #
# to deal in the Software without restriction, including without limitation  #
# the rights to use, copy, modify, merge, publish, distribute, sublicense,   #
# and/or sell copies of the Software, and to permit persons to whom the      #
# Software is furnished to do so, subject to the following conditions:       #
#                                                                            #
# The above copyright notice and this permission notice shall be included    #
# in all copies or substantial portions of the Software.                     #
#                                                                            #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR #
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   #
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    #
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER #
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    #
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        #
# DEALINGS IN THE SOFTWARE.                                                  #
##############################################################################

"""Utilities for ASTNodes, such as serialization"""

from gem.gemlang.ast.ast_base import ASTNode

__all__ = ["serialize", "format_pretty"]


def serialize(node):
    """
    Serialises an AST as text using a reverse polish notation.

    :param node: an AST node.
    """
    assert isinstance(node, ASTNode)
    string = "{node}".format(node=repr(node))
    for child in node.children:
        string += " " + serialize(child)
    return "({})".format(string)


def format_pretty(node, level=0):
    """
    Pretty prints an AST.

    :param node: an AST node :param level: the indentation level (not normally
    used outside the implementation).

    :return a string containing the pretty printed representation of the AST.
    """
    assert isinstance(node, ASTNode)
    string = (
        "    " * level
        + f"{node} [symbol={node.symbol}, scope=\
{node.scope.name if node.scope else node.scope}, eval_type={node.eval_type}]"
    )
    for child in node.children:
        string += "\n " + format_pretty(child, level + 1)
    return string
