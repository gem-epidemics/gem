##############################################################################
# Copyright 2019 Chris Jewell <c.jewell@lancaster.ac.uk>                     #
#                                                                            #
# Permission is hereby granted, free of charge, to any person obtaining a    #
# copy of this software and associated documentation files (the "Software"), #
# to deal in the Software without restriction, including without limitation  #
# the rights to use, copy, modify, merge, publish, distribute, sublicense,   #
# and/or sell copies of the Software, and to permit persons to whom the      #
# Software is furnished to do so, subject to the following conditions:       #
#                                                                            #
# The above copyright notice and this permission notice shall be included    #
# in all copies or substantial portions of the Software.                     #
#                                                                            #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR #
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   #
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    #
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER #
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    #
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        #
# DEALINGS IN THE SOFTWARE.                                                  #
##############################################################################


"""
AST Walker module
===============================================================================

The ASTWalker provides a base class to generate algorithms that walk the AST do
do stuff like symbol declaration, resolution, type-checking, and finally code
generation.
"""

import copy

from gem.gemlang.ast.ast_base import ASTNode


class StopWalk(Exception):
    """Raise this exception in an action method to abort a tree walk."""


class ASTWalker:  # pylint: disable=too-few-public-methods
    """
    Base class for all AST walking.

    This class is essentially a dispatcher for calling node-specific methods on
    entry to and exit from a node. It is a recursive descent algorithm, and is
    based on Lark's internals. Starting at the root node, each child is visited
    in turn.

    Rules are as follows:

    1. On entry to `node` of type `Foo`, if a method OnEntry_Foo(self, node)
       exists, it is called.
    2. On exiting `node` of type `Foo`, if a method OnExit_Foo(self, node)
       exists, it is called.
    3. If an `OnEntry_*` method returns non-null, the corresponding `OnExit_*`
       method is _not_ called.
    4. Whatever an `OnEntry_*` or `OnExit_*` method returns, it replaces the
       node in the children list of the parent node in the AST.

    :param in=place: Should the AST be modified in place? (default False)
    """

    def __init__(self, in_place=False):
        self.__in_place = in_place

    def __call(self, when, ast_node):
        for cls in ast_node.__class__.mro():
            attr = "{}_{}".format(when, cls.__name__)
            action = getattr(self, attr, None)
            if action is not None:
                return action(ast_node)  # pylint: disable=not-callable
        return self.__default__(ast_node)

    def __callOnEntry(self, ast_node):  # pylint: disable=invalid-name
        new_node = self.__call("onEnter", ast_node)
        if new_node is None:  # Option if a method has a "void" return type
            return ast_node
        else:
            return new_node

    def __callOnExit(self, ast_node):  # pylint: disable=invalid-name
        new_node = self.__call("onExit", ast_node)
        if new_node is None:
            return ast_node
        else:
            return new_node

    def __default__(self, ast_node):  # pylint: disable=no-self-use
        return ast_node

    def _walk(self, ast_node):
        """
        Walks an AST, dispatching methods as appropriate.

        :param ast_node: an ASTNode instance. :return an object according to
        the return values of the OnExit_* and OnEntry_* methods.
        """
        if self.__in_place is not True:
            ast_node = copy.copy(ast_node)

        try:
            ast_node = self.__callOnEntry(ast_node)
            if isinstance(
                ast_node, ASTNode
            ):  # Trap to allow re-writing to something other than an AST
                ast_node.children = [
                    self._walk(child) for child in ast_node.children
                ]
                ast_node = self.__callOnExit(ast_node)
        except StopWalk:
            # Allows us to abort a walk at the end of an onEnter
            # method for topdown parsing
            pass
        return ast_node
