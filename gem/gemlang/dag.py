##############################################################################
# Copyright 2019 Chris Jewell <c.jewell@lancaster.ac.uk>                     #
#                                                                            #
# Permission is hereby granted, free of charge, to any person obtaining a    #
# copy of this software and associated documentation files (the "Software"), #
# to deal in the Software without restriction, including without limitation  #
# the rights to use, copy, modify, merge, publish, distribute, sublicense,   #
# and/or sell copies of the Software, and to permit persons to whom the      #
# Software is furnished to do so, subject to the following conditions:       #
#                                                                            #
# The above copyright notice and this permission notice shall be included    #
# in all copies or substantial portions of the Software.                     #
#                                                                            #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR #
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   #
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    #
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER #
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    #
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        #
# DEALINGS IN THE SOFTWARE.                                                  #
##############################################################################

"""DAG generation"""


import importlib

from gem.gemlang.ast.ast_base import ASTNode
from gem.gemlang.ast.ast_statement import EpiDecl
from gem.gemlang.ast_walker import ASTWalker, StopWalk
from gem.gemlang.semantics.symbol import VariableSymbol
from gem.gemlang.semantics.symbol_table import SymbolTable

HAVE_GRAPHVIZ = importlib.util.find_spec("graphviz") is not None
if HAVE_GRAPHVIZ:
    from graphviz import Digraph


class DAGNode(ASTNode):
    """
    Represents a random variable in a probabilistic DAG.

    :param name: the name of the node :param observed: is the random variable
    observed? :type observed: bool :returns: an object of type DAGNode
    """

    def __init__(self, name, is_observed=False):
        super().__init__()
        self.__name = name
        self.__observed = is_observed
        self.__parents = []

    @property
    def name(self):
        """Returns the name of the DAGNode"""
        return self.__name

    @property
    def is_observed(self):
        """Is the DAGNode observed?"""
        return self.__observed

    @property
    def parents(self):
        """Returns a list of the DAGNode's parent DAGNodes."""
        return self.__parents

    def add_parent(self, dag_node):
        """Add a parent DAGNode"""
        assert isinstance(dag_node, DAGNode), f"'{dag_node}' is not a DAGNode"
        self.__parents.append(dag_node)

    def __repr__(self):
        string = f"<{self.__class__.__name__} {self.name}:"
        for parent in self.parents:
            string += f"  {parent}"
        return string + ">"

    def __str__(self):
        return self.name


def dag2dot(dag):
    """
    Takes a DAGNode tree and returns a string with dot code representation.

    :param dag: a DAGNode tree :returns: a string of dot code.
    """
    if not HAVE_GRAPHVIZ:
        print(Warning("Install graphviz to generate DAG visualisations"))
        return None

    dot = Digraph()
    for node in dag.values():
        dot.node(node.name)

    for node in dag.values():
        for child in node.children:
            dot.edge(node.name, child.name)
    return dot


class DAG(ASTWalker):
    """
    Creates a DAG representation of the probability model, with possible
    graphviz representation.

    Algorithm =========

    1. Walk AST
    2. For each StochasticAssignExpr: a. push declaration to a dict b.
       Recursively follow RHS, stop at IdRef nodes i. If IdRef is a
       VariableSymbol of type RandomVariable, return symbol ii. If IdRef is a
       MethodSymbol, follow definition and go to (b)
    """

    # pylint: disable=missing-function-docstring,invalid-name

    def __init__(self, ast):
        super().__init__()
        self.__dag = {}
        self.__current_rv = []
        self._walk(ast)

    def get_dag(self):
        """Returns the DAG"""
        return self.__dag

    def onEnter_StochasticAssignExpr(
        self, node
    ):  # pylint: disable=invalid-name
        rv_decl = node.children[0]
        dependent_node = DAGNode(rv_decl.value)
        self.__dag[rv_decl.value] = dependent_node
        # Push RV onto current_rv stack before walking children
        self.__current_rv.append(dependent_node)
        self._walk(node.children[1])
        # Pop RV off current_rv stack
        self.__current_rv.pop()
        raise StopWalk()

    def onEnter_IdRef(self, node):
        if (
            isinstance(node.symbol, VariableSymbol)
            and (node.symbol.attrib & SymbolTable.ATTRIB.RANDOM)
            == SymbolTable.ATTRIB.RANDOM
        ):
            parent = self.__dag[node.value]
            if len(self.__current_rv) > 0:
                child = self.__current_rv[-1]
                child.add_parent(parent)
                parent.add_child(child)
        raise StopWalk()

    def onEnter_Call(self, call):
        symb = call.children[0].symbol
        fn_def = symb.ast_def
        if isinstance(fn_def, EpiDecl):
            self._walk(fn_def.children[2])

    def dot(self):
        return dag2dot(self.__dag)
