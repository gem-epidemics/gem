##############################################################################
# Copyright 2019 Chris Jewell <c.jewell@lancaster.ac.uk>                     #
#                                                                            #
# Permission is hereby granted, free of charge, to any person obtaining a    #
# copy of this software and associated documentation files (the "Software"), #
# to deal in the Software without restriction, including without limitation  #
# the rights to use, copy, modify, merge, publish, distribute, sublicense,   #
# and/or sell copies of the Software, and to permit persons to whom the      #
# Software is furnished to do so, subject to the following conditions:       #
#                                                                            #
# The above copyright notice and this permission notice shall be included    #
# in all copies or substantial portions of the Software.                     #
#                                                                            #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR #
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   #
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    #
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER #
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    #
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        #
# DEALINGS IN THE SOFTWARE.                                                  #
##############################################################################

"""GEM gemlang code generator stage"""

import gem.gemlang.semantics.symbol
import gem.gemlang.tf_output as out
from gem.gemlang.ast.ast_base import NullNode
from gem.gemlang.ast_walker import ASTWalker
from gem.gemlang.semantics.symbol_resolve import get_random_vars
from gem.gemlang.semantics.symbol_table import SymbolTable


def error_pos(msg, meta):
    """Serialises error position"""
    return "{} at line {} column {}".format(msg, meta.line, meta.column)


def _test_flag(flags, flag_to_test):
    """Returns `True` if `flag` is set in `x`"""
    return (flags & flag_to_test) == flag_to_test


class CodeGenerator(ASTWalker):
    """
    Walks an AST, rewriting it as Outputter objects.

    :param symtab: a fully populated symbol table

    :returns: a CodeGenerator object
    """

    # pylint: disable=invalid-name,missing-docstring
    # pylint: disable=no-self-use,too-many-public-methods

    def __init__(self):
        super().__init__()
        self.__workspace_stack = []
        self.vars = []
        self.random_vars = []
        self.external_data = {}

    @property
    def current_workspace(self):
        return self.__workspace_stack[-1]

    def push_ws(self):
        self.__workspace_stack.append({})
        return self.current_workspace

    def pop_ws(self):
        return self.__workspace_stack.pop()

    def generate(self, ast, symbol_table, external_data=None):
        external_data = {} if external_data is None else external_data
        self.vars = [
            symb.name
            for symb in symbol_table.globals.get_by_type(
                gem.gemlang.semantics.symbol.VariableSymbol
            )
        ]
        self.random_vars = get_random_vars(symbol_table)
        self.external_data = external_data
        return self._walk(ast)

    def onEnter_GEMProgram(self, program):  # pylint: disable=unused-argument
        self.push_ws()

    def onExit_GEMProgram(self, output_obj):
        stmts = [out.AssignExternal(k) for k in self.external_data.keys()]
        stmts.extend(
            [
                stmt
                for stmt in output_obj.children
                if not isinstance(stmt, NullNode)
            ]
        )
        self.pop_ws()
        return str(out.GEMProgram(self.vars, stmts))

    def onEnter_Block(self, block):
        pass

    def onExit_Block(self, block):
        pass

    def onEnter_EpiDecl(self, epidecl):
        self.push_ws()
        self.current_workspace["states"] = []
        self.current_workspace["initial"] = []
        self.current_workspace["transitions"] = []
        return epidecl

    def onExit_EpiDecl(self, epidecl):
        name = epidecl.name
        config = epidecl.children[0]
        param = epidecl.children[1]
        stm = out.STMDef(
            name,
            config,
            param,
            self.current_workspace["states"],
            self.current_workspace["transitions"],
            self.current_workspace["initial"],
        )
        self.pop_ws()
        return stm

    def onEnter_Call(self, call):
        fname = self._walk(call.children[0])
        args = self._walk(call.children[1])
        return out.Function(str(fname), args)

    def onExit_FArgList(self, farglist):
        return self.onExit_ArgList(farglist)

    def onExit_ArgList(self, arglist):
        return arglist.children

    def onExit_KwArg(self, kwarg):
        return kwarg.children[1]

    def onExit_TruedivExpr(self, op):
        return out.Truediv(op.children[0], op.children[1])

    def onExit_MulExpr(self, op):
        return out.Mul(op.children[0], op.children[1])

    def onExit_MatmulExpr(self, op):
        return out.Matmul(op.children[0], op.children[1])

    def onExit_AddExpr(self, op):
        return out.Add(op.children[0], op.children[1])

    def onExit_SubExpr(self, op):
        return out.Sub(op.children[0], op.children[1])

    def onExit_PowExpr(self, op):
        return out.Pow(op.children[0], op.children[1])

    def onExit_LtExpr(self, op):
        return out.Lt(op.children[0], op.children[1])

    def onExit_GtExpr(self, op):
        return out.Gt(op.children[0], op.children[1])

    def onExit_LeqExpr(self, op):
        return out.Leq(op.children[0], op.children[1])

    def onExit_GeqExpr(self, op):
        return out.Geq(op.children[0], op.children[1])

    def onExit_EqExpr(self, op):
        return out.Eq(op.children[0], op.children[1])

    def onExit_NeExpr(self, op):
        return out.Ne(op.children[0], op.children[1])

    def onExit_NegExpr(self, op):
        return out.Neg(op.children[0])

    def onExit_NotExpr(self, op):
        return out.Not(op.children[0])

    def onEnter_AssignExpr(self, ast_node):
        lhs = ast_node.children[0]
        rhs = ast_node.children[1]
        if _test_flag(lhs.symbol.attrib, SymbolTable.ATTRIB.EXTERN):
            return NullNode("external reference removed")
        if lhs.eval_type == SymbolTable.STATE:
            initial = self._walk(rhs.children[1])
            self.current_workspace["states"].append(lhs.value)
            self.current_workspace["initial"].append(initial[0])
            return NullNode("state reference removed")
        return ast_node

    def onExit_AssignExpr(self, ast_node):
        lhs = ast_node.children[0]
        rhs = ast_node.children[1]
        return out.Assign(lhs, rhs)

    def onEnter_StochasticAssignExpr(self, ast_node):
        lhs = ast_node.children[0]
        rhs = ast_node.children[1]
        varname = lhs.value
        fname = self._walk(rhs.children[0])
        args = self._walk(rhs.children[1])
        observed = getattr(rhs, "observed", None)
        if (
            str(fname) in out.builtins_.keys()
        ):  # TODO find better solution to translating GEM distros to TF
            rv_obj = out.builtins_[str(fname)](
                args, value=observed, name=varname
            )
        else:
            rv_obj = out.STMInstance(fname, args, value=observed, name=varname)

        return out.StochasticAssign(
            varname,
            rv_obj,
            is_root_node=_test_flag(
                lhs.symbol.attrib, SymbolTable.ATTRIB.ROOT
            ),
        )

    def onExit_AssignTransition(self, ast_node):
        transition = ast_node.children[0]
        expr = ast_node.children[1]
        transition.rate = expr
        return transition

    def onExit_TransitionRef(self, txdef):
        src = txdef.children[0]
        dest = txdef.children[1]
        tx = out.Transition(src, dest)
        self.current_workspace["transitions"].append(tx)
        return tx

    def onExit_IndexExpr(self, index_expr):
        expr = index_expr.children[0]
        indices = [i.value for i in index_expr.children[1].children]
        return out.IndexExpr(expr, indices)

    def onExit_Array(self, array):
        return out.Array(array.children)

    def onExit_Number(self, n):
        return out.Number(n.value)

    def onExit_IdRef(self, ref):
        return str(ref.value)

    def onExit_String(self, string):
        return str(string.value)
