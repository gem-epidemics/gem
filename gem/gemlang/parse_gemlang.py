##############################################################################
# Copyright 2019 Chris Jewell <c.jewell@lancaster.ac.uk>                     #
#                                                                            #
# Permission is hereby granted, free of charge, to any person obtaining a    #
# copy of this software and associated documentation files (the "Software"), #
# to deal in the Software without restriction, including without limitation  #
# the rights to use, copy, modify, merge, publish, distribute, sublicense,   #
# and/or sell copies of the Software, and to permit persons to whom the      #
# Software is furnished to do so, subject to the following conditions:       #
#                                                                            #
# The above copyright notice and this permission notice shall be included    #
# in all copies or substantial portions of the Software.                     #
#                                                                            #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR #
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   #
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    #
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER #
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    #
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        #
# DEALINGS IN THE SOFTWARE.                                                  #
##############################################################################

"""Parse GEM Language to AST"""

import os

from lark import Lark, Transformer, v_args

from gem.gemlang.ast import ast_base as ast
from gem.gemlang.ast import ast_expression as expr
from gem.gemlang.ast import ast_statement as stmt


class GrammarError(Exception):
    """Raised in response to errors in gemlang grammar"""


@v_args(meta=True)
class ParseTree2AST(Transformer):
    """GEMParser parse-tree to AST transformer."""

    # pylint: disable=missing-function-docstring,no-self-use,
    # pylint: disable=unused-argument,undefined-variable,invalid-name
    # pylint: disable=too-many-public-methods

    def gem(self, stmts, meta):
        # Filter out NEWLINE tokens.
        stmts = [s for s in stmts if isinstance(s, ast.Statement)]
        return ast.GEMProgram(stmts, meta)

    def block(self, args, meta):
        return ast.Block(args, meta=meta)

    def statement(self, args, meta):
        return args[0]

    def simple_statement(self, arg, meta):
        return arg[0]

    def epidecl(self, args, meta):
        if isinstance(
            args[0], expr.ArgList
        ):  # Epidemic declaration has options
            return stmt.EpiDecl(
                str(args[1]), args[0], args[2], args[3], meta=meta
            )
        else:
            return stmt.EpiDecl(
                str(args[0]), expr.ArgList([]), args[1], args[2], meta=meta
            )

    def transitiondef(self, args, meta):
        return expr.TransitionRef(args[0], args[1], meta=meta)

    def assign_expr(self, args, meta):
        return stmt.AssignExpr(args[0], args[1], meta=meta)

    def assign_stochastic(self, args, meta):
        return stmt.StochasticAssignExpr(args[0], args[1], meta=meta)

    def assign_transition_rate(self, args, meta):
        return stmt.AssignTransition(args[0], args[1], meta=meta)

    def assign_sojourn_density(self, args, meta):
        raise NotImplementedError("Non-Markovian models not implemented yet.")
        # return StochasticAssignExpr(args[0], args[1], meta=meta)

    def expr(self, ex, meta):
        if len(ex) > 1:
            raise ValueError("Number of arguments > 1 in expression")
        return ex[0]

    def argdef(self, args, meta):
        return ast.FArg(args[0], meta=meta)

    def arglist(self, args, meta):
        return expr.ArgList(args, meta=meta)

    def argdefs(self, args, meta):
        return ast.FArgList(args, meta=meta)

    def kwarg(self, args, meta):
        kwref = expr.KwRef(args[0], meta=meta)
        expression = args[1]
        return expr.KwArg(kwref, expression, meta=meta)

    def call(self, args, meta):
        f_name = args[0]
        arg_list = args[1]
        return expr.Call(f_name, arg_list, meta=meta)

    def indexlist(self, args, meta):
        return ast.IndexList(args, meta=meta)

    def indexop(self, args, meta):
        return expr.IndexExpr(args[0], args[1], meta=meta)

    def eq(self, args, meta):
        return expr.EqExpr(args[0], args[1], meta=meta)

    def ne(self, args, meta):
        return expr.NeExpr(args[0], args[1], meta=meta)

    def geq(self, args, meta):
        return expr.GeqExpr(args[0], args[1], meta=meta)

    def leq(self, args, meta):
        return expr.LeqExpr(args[0], args[1], meta=meta)

    def gt(self, args, meta):
        return expr.GtExpr(args[0], args[1], meta=meta)

    def lt(self, args, meta):
        return expr.LtExpr(args[0], args[1], meta=meta)

    def add(self, args, meta):
        return expr.AddExpr(args[0], args[1], meta=meta)

    def mul(self, args, meta):
        return expr.MulExpr(args[0], args[1], meta=meta)

    def matmul(self, args, meta):
        return expr.MatmulExpr(args[0], args[1], meta=meta)

    def sub(self, args, meta):
        return expr.SubExpr(args[0], args[1], meta=meta)

    def truediv(self, args, meta):
        return expr.TruedivExpr(args[0], args[1], meta=meta)

    def not_(self, args, meta):
        return expr.Not_Expr(args[0], meta=meta)

    def neg(self, args, meta):
        return expr.NegExpr(args[0], meta=meta)

    def pow(self, args, meta):
        return expr.PowExpr(args[0], args[1], meta=meta)

    def idref(self, args, meta):
        return expr.IdRef(str(args[0]), meta=meta)

    def array(self, args, meta):
        return expr.Array(args, meta=meta)

    def number(self, args, meta):
        if args[0].type == "INT":
            return expr.Integer(args[0], meta=meta)
        if args[0].type == "FLOAT":
            return expr.Float(args[0], meta=meta)
        raise TypeError(
            f"Unrecognised Numeric type at line {meta.line} \
column {meta.column}."
        )

    def string(self, args, meta):
        return expr.String(str(args[0][1:-1]), meta=meta)

    def __default__(self, data, children, meta):
        raise GrammarError(
            f"No translation of production rule '{data}' in GEM grammar"
        )


class GEMParser:
    """Instantiates a gemlang 1st stage parser."""

    __glpath = os.path.dirname(__file__)
    with open(os.path.join(__glpath, "gem_grammar.cfgr"), "r") as f:
        __grammar = f.read()
    __parser = Lark(
        __grammar, start="gem", parser="earley", propagate_positions=True
    )

    def __new__(cls, program):
        parse_tree = cls.parse(program)
        return ParseTree2AST().transform(parse_tree)

    @classmethod
    def parse(cls, program):
        """Returns a Lark parse tree representation of a gem program."""
        return cls.__parser.parse(program)
