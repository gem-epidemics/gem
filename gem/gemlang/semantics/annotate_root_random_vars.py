##############################################################################
# Copyright 2019 Chris Jewell <c.jewell@lancaster.ac.uk>                     #
#                                                                            #
# Permission is hereby granted, free of charge, to any person obtaining a    #
# copy of this software and associated documentation files (the "Software"), #
# to deal in the Software without restriction, including without limitation  #
# the rights to use, copy, modify, merge, publish, distribute, sublicense,   #
# and/or sell copies of the Software, and to permit persons to whom the      #
# Software is furnished to do so, subject to the following conditions:       #
#                                                                            #
# The above copyright notice and this permission notice shall be included    #
# in all copies or substantial portions of the Software.                     #
#                                                                            #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR #
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   #
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    #
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER #
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    #
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        #
# DEALINGS IN THE SOFTWARE.                                                  #
##############################################################################

"""Walk AST, annotate symbol denoting parentless probabilisitic DAG nodes"""

from gem.gemlang.ast_walker import ASTWalker
from gem.gemlang.dag import DAG
from gem.gemlang.semantics.symbol_table import SymbolTable


class AnnotateRootDAGNodes(ASTWalker):
    """Walk the AST, annotating root random variable symbols"""

    def __init__(self):
        self.__dag = {}
        super().__init__(in_place=True)

    def visit(self, ast_node, symtab=None):  # pylint: disable=unused-argument
        """
        Walks the AST and constructs a symbol table of declared symbols. Each
        symbol is annotated with a pointer to its respective declaration in the
        AST.

        :param ast_node: an AST root node :returns: a populated SymbolTable
        corresponding to ast_node
        """
        self.__dag = DAG(ast_node).get_dag()
        self._walk(ast_node)

    def onExit_StochasticAssignExpr(self, node):
        """
        Capture StochasticAssignExpr, and mark node as Root if it does not
        depend on other RandomVariables
        """

        lhs = node.children[0]
        if len(self.__dag[lhs.value].parents) == 0:
            node.children[0].symbol.attrib |= SymbolTable.ATTRIB.ROOT
