##############################################################################
# Copyright 2019 Chris Jewell <c.jewell@lancaster.ac.uk>                     #
#                                                                            #
# Permission is hereby granted, free of charge, to any person obtaining a    #
# copy of this software and associated documentation files (the "Software"), #
# to deal in the Software without restriction, including without limitation  #
# the rights to use, copy, modify, merge, publish, distribute, sublicense,   #
# and/or sell copies of the Software, and to permit persons to whom the      #
# Software is furnished to do so, subject to the following conditions:       #
#                                                                            #
# The above copyright notice and this permission notice shall be included    #
# in all copies or substantial portions of the Software.                     #
#                                                                            #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR #
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   #
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    #
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER #
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    #
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        #
# DEALINGS IN THE SOFTWARE.                                                  #
##############################################################################

"""Scope table classes."""

from collections import OrderedDict

from gem.gemlang.ast.ast_base import ASTNode


class Scope:
    """Base class interface for a scope"""

    def __init__(self, enclosing_scope, **kwargs):
        super().__init__(**kwargs)
        self.__enclosing_scope = enclosing_scope
        self.__symbols = OrderedDict()
        self.__level = (
            enclosing_scope.level + 1 if enclosing_scope is not None else 0
        )
        self.__ast_def = None

    @property
    def ast_def(self):
        """Returns a reference to the AST where the scope was defined."""
        return self.__ast_def

    @ast_def.setter
    def ast_def(self, ast_def):
        """Sets a reference to the ASTNode where the scope is defined."""
        assert isinstance(ast_def, ASTNode)
        self.__ast_def = ast_def

    @property
    def parent_scope(self):
        """
        Where to look next for symbols. :returns: an instance of a Scope
        """
        return self.enclosing_scope

    @property
    def enclosing_scope(self):
        """
        Returns the Scope in which this Scope is defined. :returns: an instance
        of Scope, or None if global scope.
        """
        return self.__enclosing_scope

    @property
    def level(self):
        """
        Returns the scope level relative to a root scope. :returns: an integer
        giving the scope level (0-based)
        """
        return self.__level

    def declare(self, symbol):
        """
        Symbol declaration

        :param symbol: a :class:Symbol object

        :returns: None

        :raises: KeyError if symbol is already declared (duplicate declaration)
        """
        if symbol.name in self.__symbols:
            raise NameError("Duplicate declaration '{}'".format(symbol.name))
        self.__symbols[symbol.name] = symbol
        symbol.scope = self

    def get_members(self):
        return self.__symbols

    def resolve(self, symbol_name):
        """
        Resolves `symbol_name` in the scope.

        :param symbol_name: the name of the symbol.

        :returns: an object of type Symbol if found.

        :raises: KeyError if symbol not found in the scope hierarchy.
        """
        symbol = self.__symbols.get(symbol_name)
        if symbol is not None:
            return symbol
        elif self.parent_scope is not None:
            return self.parent_scope.resolve(symbol_name)
        else:
            raise NameError("Undefined symbol '{}'".format(symbol_name))

    def get_by_type(self, symbol_type):
        """
        Returns all symbols of a given type

        :param symbol_type: a symbol class name

        :return a list of symbols of type symbol_type
        """
        return [
            symb
            for symb in self.__symbols.values()
            if isinstance(symb, symbol_type)
        ]

    def __str__(self):
        symtab_header = "{} scope symbol table".format(self.__class__)
        lines = [symtab_header]
        lines.extend(
            ("%s%s: %r" % (" " * self.__level, key, value))
            for key, value in self.__symbols.items()
        )
        string = "\n".join(lines)
        return string


class GlobalScope(Scope):
    """A global scope."""

    def __init__(self):
        super().__init__(enclosing_scope=None)

    @property
    def name(self):
        """Returns the name of the scope"""
        return "global"


class LocalScope(Scope):
    """
    A local scope

    :param enclosing_scope: the parent scope
    """

    def __init__(self, enclosing_scope=None):
        assert enclosing_scope is not None
        super().__init__(enclosing_scope)

    @property
    def name(self):
        """Returns the name of the scope"""
        return "local"
