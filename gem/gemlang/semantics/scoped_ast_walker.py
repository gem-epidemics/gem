##############################################################################
# Copyright 2019 Chris Jewell <c.jewell@lancaster.ac.uk>                     #
#                                                                            #
# Permission is hereby granted, free of charge, to any person obtaining a    #
# copy of this software and associated documentation files (the "Software"), #
# to deal in the Software without restriction, including without limitation  #
# the rights to use, copy, modify, merge, publish, distribute, sublicense,   #
# and/or sell copies of the Software, and to permit persons to whom the      #
# Software is furnished to do so, subject to the following conditions:       #
#                                                                            #
# The above copyright notice and this permission notice shall be included    #
# in all copies or substantial portions of the Software.                     #
#                                                                            #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR #
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   #
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    #
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER #
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    #
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        #
# DEALINGS IN THE SOFTWARE.                                                  #
##############################################################################

"""Base class for an AST walker that has scope management."""

from gem.gemlang.ast_walker import ASTWalker
from gem.gemlang.semantics.scope import Scope


class ScopedWalker(ASTWalker):
    """Base class for AST Walkers needing scope management"""

    # pylint: disable=too-few-public-methods

    def __init__(self):
        super().__init__(in_place=True)
        self._scope_stack = []

    @property
    def current_scope(self):
        """Returns a reference to the current scope"""
        return self._scope_stack[-1]

    def _push_scope(self, scope):
        """Pushes a new scope onto the scope stack"""
        assert isinstance(
            scope, Scope
        ), f"Cannot push scope. Object is '{scope}'.  Current scope is \
'{self.current_scope.name}'"
        self._scope_stack.append(scope)
        return scope

    def _pop_scope(self):
        """Pops a scope from the scope stack."""
        return self._scope_stack.pop()
