##############################################################################
# Copyright 2019 Chris Jewell <c.jewell@lancaster.ac.uk>                     #
#                                                                            #
# Permission is hereby granted, free of charge, to any person obtaining a    #
# copy of this software and associated documentation files (the "Software"), #
# to deal in the Software without restriction, including without limitation  #
# the rights to use, copy, modify, merge, publish, distribute, sublicense,   #
# and/or sell copies of the Software, and to permit persons to whom the      #
# Software is furnished to do so, subject to the following conditions:       #
#                                                                            #
# The above copyright notice and this permission notice shall be included    #
# in all copies or substantial portions of the Software.                     #
#                                                                            #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR #
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   #
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    #
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER #
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    #
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        #
# DEALINGS IN THE SOFTWARE.                                                  #
##############################################################################

"""gemlang Symbol classes"""

from collections import OrderedDict
from enum import Flag, auto

from gem.gemlang.semantics.scope import Scope

__all__ = [
    "BuiltinTypeSymbol",
    "VariableSymbol",
    "MethodSymbol",
    "STMSymbol",
    "Symbol",
    "Type",
    "SYMBOL_ATTR",
    "ExternalDataSymbol",
    "ScopedSymbol",
    "UnresolvedSymbolError",
]


class Type:  # pylint: disable=too-few-public-methods
    """
    Empty class to act as a tag
    """


class UnresolvedSymbolError(Exception):
    """Thrown if a symbol is not found."""


class SYMBOL_ATTR(Flag):  # pylint: disable=invalid-name
    """Symbol attribute enum"""

    DEFAULT = auto()
    EXTERN = auto()  # references data external to the GEM model_generator
    ROOT = auto()  # parentless random variable (i.e. prior distribution)
    RANDOM = auto()  # Random variable attribute


class Symbol:
    """Represents a symbol."""

    def __init__(self, name, symbol_type=None, **kwargs):
        """
        The Symbol base class, used for representing a symbol in a GEM program.

        :param name: the name of the symbol
        :param symbol_type: the return type of the symbol.

        :returns: an object of type :class:`Symbol`
        """

        super().__init__(**kwargs)
        self.__name = str(name)
        self.__type = symbol_type
        self.__scope = None
        self.__ast_def = None
        self.__attrib = SYMBOL_ATTR.DEFAULT

    @property
    def name(self):
        """Returns the name of the symbol"""
        return self.__name

    @property
    def type(self):
        """Returns the symbol type"""
        return self.__type

    @type.setter
    def type(self, symbol_type):
        """Sets the symbol type"""
        self.__type = symbol_type

    @property
    def ast_def(self):
        return self.__ast_def

    @ast_def.setter
    def ast_def(self, ast_node):
        self.__ast_def = ast_node

    @property
    def attrib(self):
        """
        Returns symbol attributes

        :returns: instance of :class:SYMBOL_ATTR
        """
        return self.__attrib

    @attrib.setter
    def attrib(self, attrib):
        """
        Sets symbol attributes

        :param attrib: attributes
        :type attrib: :class:SYMBOL_ATTR
        """
        self.__attrib = attrib

    @property
    def scope(self):
        """Returns the scope the symbol was defined in"""
        return self.__scope

    @scope.setter
    def scope(self, scope):
        """Sets the scope the symbol was defined in"""
        self.__scope = scope

    def __str__(self):
        return self.name

    def __repr__(self):
        return f"<{self.__class__.__name__} '{self.name}' [{self.type}]>"


class ExternalDataSymbol(Symbol):
    """
    A symbol referring to external data.

    :param name: the name of the symbol
    :param type: the external data type
    """


class BuiltinTypeSymbol(Symbol, Type):
    def __init__(self, name: str, symbol_type: Symbol = None) -> None:
        """
        A builtin symbol

        :param name: the name of the symbol
        :param symbol_type: the return type of the symbol
        """
        super().__init__(name, symbol_type)


class VariableSymbol(Symbol):
    """
    A symbol representing a variable

    :param name: the name of the symbol
    :param definition: a reference to the AST where the symbol is defined
    :param symbol_type: the variable type
    """

    def __init__(self, name: str, symbol_type: Symbol = None) -> None:
        super().__init__(name, symbol_type)


class ScopedSymbol(Symbol, Scope):
    """Base class for a scoped symbol."""

    def __init__(self, name, symbol_type, enclosing_scope):
        super().__init__(
            name=name, symbol_type=symbol_type, enclosing_scope=enclosing_scope
        )
        self.__enclosing_scope = enclosing_scope
        self.__level = enclosing_scope.level + 1

    @property
    def level(self):
        return self.__level

    @property
    def parent_scope(self):
        return self.enclosing_scope

    @property
    def enclosing_scope(self):
        return self.__enclosing_scope

    def resolve(self, symbol_name):
        sym = self.get_members().get(symbol_name)
        if sym is not None:
            return sym
        if self.parent_scope is not None:
            return self.parent_scope.resolve(symbol_name)
        return None

    def declare(self, symbol):
        if symbol.name in self.get_members():
            raise NameError("Duplicate declaration '{}'".format(symbol.name))
        self.get_members()[symbol.name] = symbol
        symbol.scope = self

    def __str__(self):
        symtab_header = "{} scope symbol table".format(self.__class__)
        lines = [symtab_header]
        lines.extend(
            ("%s%s: %r" % (" " * self.__level, key, value))
            for key, value in self.get_members().items()
        )
        string = "\n".join(lines)
        return string


class MethodSymbol(ScopedSymbol):
    """
    Represents a function scope.

    :param name: name of the function.
    :param return_type: symbol denoting the return type
    :param enclosing_scope: the enclosing scope
    """

    def __init__(self, name, return_type, enclosing_scope):
        super().__init__(name, return_type, enclosing_scope)
        self.__ordered_args = OrderedDict()

    def get_members(self):
        """Returns an OrderedDict of formal arguments"""
        return self.__ordered_args

    def resolve_arg(self, symbol_name):
        """Resolves arguments, without recursing to the parent scope"""
        return self.get_members().get(symbol_name)

    @property
    def argcount(self):
        """Returns the number of formal arguments taken by the function."""
        return len(self.__ordered_args)


class STMSymbol(ScopedSymbol, Type):
    """
    A State Transition Model scope, used to represent an epidemic.

    :param name: the name of the STM.
    :param enclosing_scope: the enclosing scope.
    :param superclass: **Future implementation** model inheritance
    """

    def __init__(self, name, enclosing_scope, superclass=None):
        super().__init__(
            name=str(name), symbol_type=None, enclosing_scope=enclosing_scope
        )

        self.__builtins = self.__make_builtins()
        for sym in self.__builtins:
            self.declare(sym)

        self.type = "RandomVariable"

    def __make_builtins(self):
        return [
            VariableSymbol("type", "String"),
            VariableSymbol("time_origin", "Scalar"),
            VariableSymbol("time_step", "Scalar"),
            _make_method_symbol(
                "State",
                self.enclosing_scope.resolve("StateType"),
                ["init"],
                enclosing_scope=self,
            ),
        ]

    def resolve(self, symbol_name):
        sym = self.get_members().get(symbol_name)
        if sym is not None:
            return sym
        if self.parent_scope is not None:
            return self.parent_scope.resolve(symbol_name)
        return None

    @property
    def argcount(self):
        """Returns the number of arguments taken my the model constructor."""
        return len(self.get_members()) - len(self.__builtins)


def _make_method_symbol(name, return_type, args, enclosing_scope):
    symbol = MethodSymbol(
        name=name, return_type=return_type, enclosing_scope=enclosing_scope
    )
    for arg in args:
        symbol.declare(VariableSymbol(arg, None))
    return symbol
