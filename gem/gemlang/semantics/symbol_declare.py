##############################################################################
# Copyright 2019 Chris Jewell <c.jewell@lancaster.ac.uk>                     #
#                                                                            #
# Permission is hereby granted, free of charge, to any person obtaining a    #
# copy of this software and associated documentation files (the "Software"), #
# to deal in the Software without restriction, including without limitation  #
# the rights to use, copy, modify, merge, publish, distribute, sublicense,   #
# and/or sell copies of the Software, and to permit persons to whom the      #
# Software is furnished to do so, subject to the following conditions:       #
#                                                                            #
# The above copyright notice and this permission notice shall be included    #
# in all copies or substantial portions of the Software.                     #
#                                                                            #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR #
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   #
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    #
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER #
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    #
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        #
# DEALINGS IN THE SOFTWARE.                                                  #
##############################################################################

"""Symbol declaration step."""

from gem.gemlang.ast.ast_base import NullNode
from gem.gemlang.semantics.scope import LocalScope
from gem.gemlang.semantics.scoped_ast_walker import ScopedWalker
from gem.gemlang.semantics.symbol import STMSymbol, VariableSymbol
from gem.gemlang.semantics.symbol_table import SymbolTable


class SymbolDeclarer(ScopedWalker):
    """Class implementing symbol declaration on an AST"""

    # pylint: disable=missing-function-docstring,no-self-use
    # pylint: disable=unused-argument,invalid-name

    def visit(self, ast_node, symbol_table):
        """
        Walks the AST and constructs a symbol table of declared symbols. Each
        symbol is annotated with a pointer to its respective declaration in the
        AST.

        :param ast_node: an AST root node

        :returns: a populated SymbolTable corresponding to `ast_node`
        """

        self._push_scope(symbol_table.globals)
        self._walk(ast_node)

    def onEnter_GEMProgram(self, ast_node):
        self.current_scope.ast_def = ast_node

    def onExit_GEMProgram(self, ast_node):
        return ast_node

    def onExit_AssignExpr(self, assign):
        var = assign.children[0]
        varname = str(var.value)

        # Search for external symbol
        try:
            sym = self.current_scope.resolve(varname)
            if (
                sym.attrib & SymbolTable.ATTRIB.EXTERN
            ) == SymbolTable.ATTRIB.EXTERN:
                return NullNode(f"Extern ref to {sym}")
            else:
                raise SyntaxError(
                    f"Duplicate declaration of '{varname}' at line {assign.meta.line} \
                    column {assign.meta.column}"
                )
        except NameError:
            pass

        sym = VariableSymbol(name=varname)
        self.current_scope.declare(sym)
        sym.ast_def = var
        var.symbol = sym
        var.scope = self.current_scope

    def onExit_StochasticAssignExpr(self, assign):
        var = assign.children[0]
        varname = str(var.value)
        sym = VariableSymbol(varname)
        sym.attrib |= SymbolTable.ATTRIB.RANDOM
        try:
            self.current_scope.declare(sym)
        except NameError as exc:
            raise SyntaxError(
                f"Duplicate declaration of '{varname}' at \
                line {assign.meta.line} column {assign.meta.column}"
            ) from exc
        sym.ast_def = var
        var.symbol = sym
        var.scope = self.current_scope

    def onEnter_EpiDecl(self, epidecl):
        sym = STMSymbol(name=epidecl.name, enclosing_scope=self.current_scope)
        self.current_scope.declare(sym)
        self._push_scope(sym)
        sym.ast_def = epidecl
        epidecl.symbol = sym
        epidecl.scope = self.current_scope

    def onExit_EpiDecl(self, epidecl):
        self._pop_scope()

    def onExit_FArg(self, argdef):
        sym = VariableSymbol(name=argdef.value)
        sym.ast_def = argdef
        self.current_scope.declare(sym)
        argdef.symbol = sym
        argdef.scope = self.current_scope

    def onEnter_Block(self, block):
        scope = LocalScope(enclosing_scope=self.current_scope)
        scope.ast_def = block
        block.symbol = scope
        block.scope = self.current_scope
        self._push_scope(scope)

    def onExit_Block(self, block):
        self._pop_scope()
