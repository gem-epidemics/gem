##############################################################################
# Copyright 2019 Chris Jewell <c.jewell@lancaster.ac.uk>                     #
#                                                                            #
# Permission is hereby granted, free of charge, to any person obtaining a    #
# copy of this software and associated documentation files (the "Software"), #
# to deal in the Software without restriction, including without limitation  #
# the rights to use, copy, modify, merge, publish, distribute, sublicense,   #
# and/or sell copies of the Software, and to permit persons to whom the      #
# Software is furnished to do so, subject to the following conditions:       #
#                                                                            #
# The above copyright notice and this permission notice shall be included    #
# in all copies or substantial portions of the Software.                     #
#                                                                            #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR #
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   #
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    #
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER #
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    #
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        #
# DEALINGS IN THE SOFTWARE.                                                  #
##############################################################################

"""GEM symbol resolution algorithms"""

from collections import namedtuple

import gem.gemlang.semantics.symbol
from gem.gemlang.semantics.scoped_ast_walker import ScopedWalker
from gem.gemlang.semantics.symbol_table import SymbolTable


class SymbolResolver(ScopedWalker):
    """GEM Symbol Resolver class."""

    # pylint: disable=missing-function-docstring,no-self-use,
    # pylint: disable=unused-argument,invalid-name

    def __init__(self):
        super().__init__()
        self._current_function_scope = None

    def visit(self, ast_node, symbol_table):
        """
        Resolves symbols in ast_node against symbol_table. ast_node is modified
        in-place, each reference to a symbol being annotated with a reference
        to the symbol in the symbol table.

        :param ast_node: an AST root node
        :param symbol_table: a SymbolTable to resolve `ast_node` against

        :returns: Nothing. `ast_node` modified in place.
        """
        self._push_scope(symbol_table.globals)
        self._walk(ast_node)

    def onEnter_EpiDecl(self, ast_node):
        self._push_scope(ast_node.symbol)
        self._current_function_scope = ast_node.symbol

    def onExit_EpiDecl(self, ast_node):
        self._pop_scope()
        self._current_function_scope = None
        try:
            sym = self.current_scope.resolve(ast_node.name)
        except NameError as e:
            raise NameError(
                f"{e} at line {ast_node.meta.line} column \
{ast_node.meta.column}"
            ) from e
        ast_node.symbol = sym

    def onEnter_Block(self, ast_node):
        self._push_scope(ast_node.symbol)

    def onExit_Block(self, ast_node):
        self._pop_scope()

    def onEnter_Call(self, call):
        try:
            idref = call.children[0]
            fsym = self.current_scope.resolve(idref.value)
            self._current_function_scope = fsym
        except NameError as err:
            raise NameError(
                f"Undefined function '{idref.value}' at line \
{call.meta.line} column {call.meta.column}."
            ) from err

    def onExit_Call(self, call):
        # 1. Keyword argument names have to be resolved in the
        #    function call's scope
        # 2. Keyword argument values have to be resolved in the
        #    current enclosing scope
        f_idref = call.children[0]
        arglist = call.children[1]
        self._current_function_scope = None
        if len(arglist.children) != f_idref.symbol.argcount:
            raise SyntaxError(
                f"Missing required argument at line \
{call.meta.line} column {call.meta.line}."
            )

    def onExit_IdRef(self, ast_node):
        try:
            sym = self.current_scope.resolve(ast_node.value)
        except NameError as e:
            raise NameError(
                "{} at line {} column {}".format(
                    e, ast_node.meta.line, ast_node.meta.column
                )
            ) from e
        ast_node.symbol = sym
        ast_node.scope = sym.scope  # self.current_scope

    def onExit_KwRef(self, node):
        try:
            sym = self._current_function_scope.get_members().get(node.value)
        except NameError as e:
            raise NameError(f"No named argument '{node.value}'") from e
        node.symbol = sym
        node.scope = sym.scope


RandomVariableList = namedtuple("RandomVariableList", ["free", "observed"])


def get_random_vars(symtab):
    """
    Scrapes a symbol table for RandomVariableSymbols, looking at the associated
    ASTNode to decide if the RV is observed or not.

    This function currently only works at the top level of a hierarchically
    scoped symbol table.

    :param symtab: a SymbolTable object where symbols have ASTs associated with
                   them.

    :return a NamedTuple containing free and observed random variables.
    """

    free_rvs = []
    observed_rvs = []
    symbols = [
        sym
        for sym in symtab.globals.get_by_type(
            gem.gemlang.semantics.symbol.VariableSymbol
        )
        if sym.type == SymbolTable.RANDOM_VARIABLE
    ]
    for symbol in symbols:
        ast = symbol.ast_def
        assert (
            ast is not None
        ), f"symbol {symbol} is not annotated with an ASTNode"
        observed = getattr(ast, "observed", None)
        if observed is not None:
            observed_rvs.append(symbol.name)
        else:
            free_rvs.append(symbol.name)
    return tuple(free_rvs)
