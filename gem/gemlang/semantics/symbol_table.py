##############################################################################
# Copyright 2019 Chris Jewell <c.jewell@lancaster.ac.uk>                     #
#                                                                            #
# Permission is hereby granted, free of charge, to any person obtaining a    #
# copy of this software and associated documentation files (the "Software"), #
# to deal in the Software without restriction, including without limitation  #
# the rights to use, copy, modify, merge, publish, distribute, sublicense,   #
# and/or sell copies of the Software, and to permit persons to whom the      #
# Software is furnished to do so, subject to the following conditions:       #
#                                                                            #
# The above copyright notice and this permission notice shall be included    #
# in all copies or substantial portions of the Software.                     #
#                                                                            #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR #
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   #
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    #
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER #
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    #
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        #
# DEALINGS IN THE SOFTWARE.                                                  #
##############################################################################

"""GEM Symbol Table implementation"""

from gem.gemlang.semantics.scope import GlobalScope
from gem.gemlang.semantics.symbol import (
    SYMBOL_ATTR,
    BuiltinTypeSymbol,
    VariableSymbol,
    _make_method_symbol,
)


class SymbolTable:
    """A GEM Symbol Table"""

    BOOLEAN = BuiltinTypeSymbol("boolean")
    CHAR = BuiltinTypeSymbol("char")
    INT = BuiltinTypeSymbol("int")
    FLOAT = BuiltinTypeSymbol("float")
    STATE = BuiltinTypeSymbol("StateType", FLOAT)
    RANDOM_VARIABLE = BuiltinTypeSymbol("RandomVariable", FLOAT)

    ATTRIB = SYMBOL_ATTR

    def __init__(self):
        self.__globals = GlobalScope()
        self.__init_type_system()

    @property
    def globals(self):
        """Returns global scope"""
        return self.__globals

    def __init_type_system(self):
        global_scope = self.globals
        global_scope.declare(SymbolTable.BOOLEAN)
        global_scope.declare(SymbolTable.CHAR)
        global_scope.declare(SymbolTable.INT)
        global_scope.declare(SymbolTable.FLOAT)
        global_scope.declare(SymbolTable.RANDOM_VARIABLE)
        global_scope.declare(SymbolTable.STATE)
        global_scope.declare(BuiltinTypeSymbol("NDArrayType"))

        global_scope.declare(
            _make_method_symbol(
                "Gamma",
                "RandomVariable",
                ["shape", "scale"],
                enclosing_scope=global_scope,
            )
        )
        global_scope.declare(
            _make_method_symbol(
                "Normal",
                "RandomVariable",
                ["mean", "stddev"],
                enclosing_scope=global_scope,
            )
        )
        global_scope.declare(
            _make_method_symbol(
                "Scalar", None, [], enclosing_scope=global_scope
            )
        )
        global_scope.declare(
            _make_method_symbol(
                "Vector", None, [], enclosing_scope=global_scope
            )
        )
        global_scope.declare(
            _make_method_symbol(
                "Matrix", None, [], enclosing_scope=global_scope
            )
        )
        global_scope.declare(
            _make_method_symbol(
                "NDArray", None, [], enclosing_scope=global_scope
            )
        )
        global_scope.declare(
            _make_method_symbol(
                "exp", None, ["x"], enclosing_scope=global_scope
            )
        )
        global_scope.declare(
            _make_method_symbol(
                "matvec", None, ["a", "b"], enclosing_scope=global_scope
            )
        )

    def declare_extern(self, var_dict: dict) -> None:
        """Declares external data"""
        for key in var_dict.keys():
            sym = VariableSymbol(name=key)
            self.globals.declare(sym)
            sym.attrib |= SymbolTable.ATTRIB.EXTERN

    def __str__(self):
        return str(self.globals)
