##############################################################################
# Copyright 2019 Chris Jewell <c.jewell@lancaster.ac.uk>                     #
#                                                                            #
# Permission is hereby granted, free of charge, to any person obtaining a    #
# copy of this software and associated documentation files (the "Software"), #
# to deal in the Software without restriction, including without limitation  #
# the rights to use, copy, modify, merge, publish, distribute, sublicense,   #
# and/or sell copies of the Software, and to permit persons to whom the      #
# Software is furnished to do so, subject to the following conditions:       #
#                                                                            #
# The above copyright notice and this permission notice shall be included    #
# in all copies or substantial portions of the Software.                     #
#                                                                            #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR #
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   #
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    #
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER #
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    #
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        #
# DEALINGS IN THE SOFTWARE.                                                  #
##############################################################################

"""Resolve symbol types"""

from warnings import warn

from gem.gemlang.semantics import ScopedWalker, SymbolTable


class TypeResolver(ScopedWalker):

    # pylint: disable=missing-function-docstring,no-self-use,invalid-name
    def visit(self, ast_node, symbol_table):
        """
        This scoped tree walker performs type resolution.

        AST nodes of type Expr will be annotated with the type symbol denoting
        what the expression evaluates to.

        This class expects that all symbol AST nodes (IdRef, EpiDecl) have been
        annotated with their respective symbol where appropriate.

        :param ast_node: an AST root node :param symbol_table: a SymbolTable to
        resolve against.
        """

        self._push_scope(symbol_table.globals)
        self._walk(ast_node)

    def onExit_Integer(self, integer):
        integer.eval_type = SymbolTable.INT

    def onExit_Float(self, node):
        node.eval_type = SymbolTable.FLOAT

    def onExit_Boolean(self, node):
        node.eval_type = SymbolTable.BOOLEAN

    def onExit_BinaryExpr(self, node):
        node.eval_type = node.children[0].eval_type

    def onExit_UnaryExpr(self, node):
        node.eval_type = node.children[0].eval_type

    def onExit_Call(self, call):
        symbol = call.children[0].symbol
        call.eval_type = symbol.type

    def onExit_AssignExpr(self, node):
        lhs, rhs = node.children

        if rhs.eval_type is None:
            warn(
                f"Unknown expression type at line {node.meta.line} column \
{node.meta.column}"
            )

        lhs.eval_type = rhs.eval_type
        lhs.symbol.type = rhs.eval_type

    def onExit_StochasticAssignExpr(self, node):
        lhs, rhs = node.children

        assert (
            rhs.eval_type is not None
        ), f"Unknown expression type at line {node.meta.line} column \
{node.meta.column}"

        lhs.eval_type = rhs.eval_type
        lhs.symbol.type = rhs.eval_type

    def onExit_IdRef(self, node):
        node.eval_type = node.symbol.type

    def onExit_KwArg(self, node):
        lhs, rhs = node.children
        lhs.eval_type = rhs.eval_type
        lhs.symbol.type = rhs.eval_type
