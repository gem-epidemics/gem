##############################################################################
# Copyright 2019 Chris Jewell <c.jewell@lancaster.ac.uk>                     #
#                                                                            #
# Permission is hereby granted, free of charge, to any person obtaining a    #
# copy of this software and associated documentation files (the "Software"), #
# to deal in the Software without restriction, including without limitation  #
# the rights to use, copy, modify, merge, publish, distribute, sublicense,   #
# and/or sell copies of the Software, and to permit persons to whom the      #
# Software is furnished to do so, subject to the following conditions:       #
#                                                                            #
# The above copyright notice and this permission notice shall be included    #
# in all copies or substantial portions of the Software.                     #
#                                                                            #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR #
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   #
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    #
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER #
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    #
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        #
# DEALINGS IN THE SOFTWARE.                                                  #
##############################################################################

import tensorflow as tf


def convert_to_maths_layer(data_dict):
    for k, v in data_dict.items():
        if hasattr(v, "to_tensorflow"):
            data_dict[k] = v.to_tensorflow()
        else:
            data_dict[k] = tf.convert_to_tensor(v, name=k)
    return data_dict


class Outputter:
    def __init__(self, indent_level=0, indent_amount=4):
        self.indent_level = indent_level
        self.indent_amount = indent_amount
        self.content = ""

    def indent(self, incr=1):
        self.indent_level += incr

    def dedent(self, incr=1):
        # assert self.indent_level > 0
        self.indent_level -= incr

    def line(self, s):
        s_out = " " * self.indent_level * self.indent_amount
        s_out += s
        s_out += "\n"
        return s_out

    def to_python(self, indent_incr=0):
        self.indent(indent_incr)
        s = self.line(self.__str__())
        self.dedent(indent_incr)
        return s


def dictify_vars(vars):
    dict_strs = [f"'{p}': {p}" for p in vars]
    return f"{{{', '.join(dict_strs)}}}"


class GEMProgram(Outputter):
    def __init__(self, vars, stmts, **kwargs):
        super().__init__(**kwargs)
        self.vars = vars
        self.stmts = stmts

    def __gen_header(self):
        str = (
            self.line("### Start of GEM-generated code ###")
            + self.line("import numpy as np")
            + self.line("import tensorflow as tf")
            + self.line("import tensorflow_probability as tfp")
            + self.line("from gemlib import distributions as gld")
            + self.line("tfd = tfp.distributions")
            + self.line("Root = tfd.JointDistributionCoroutine.Root")
        )
        return str

    def __gen_footer(self):
        footer = self.line("### End of GEM-generated code ###")
        return footer

    def __str__(self):
        s = self.__gen_header()
        s += self.line("def model_impl_fn():")
        self.indent()
        for stmt in self.stmts:
            assert isinstance(
                stmt, Outputter
            ), f"Received object of type {type(stmt)}"
            s += f"{stmt.to_python(self.indent_level)}"
        self.dedent()
        s += self.line(
            "model_impl = tfd.JointDistributionCoroutineAutoBatched(\
model_impl_fn)"
        )  # Convert to named tuple
        s += self.__gen_footer()
        return s


class STMDef(Outputter):
    def __init__(
        self,
        name,
        config,
        param,
        states,
        transitions,
        initial_states,
        **kwargs,
    ):
        super().__init__(**kwargs)
        self.name = name
        self.config = config
        self.param = param
        self.states = states
        self.transitions = transitions
        self.inits = initial_states
        self.initial_step = 0
        self.time_delta = 1.0
        self.num_steps = 100

    def stoichiometry(self):
        s = "["
        for tx in self.transitions:
            s += "["
            for state in self.states:
                if str(tx.src) == state:
                    s += "-1,"
                elif str(tx.dest) == state:
                    s += " 1,"
                else:
                    s += " 0,"
            s += "],"
        s += "]"
        return s

    def initial(self):
        """Writes the initial values matrix"""
        s = "["
        for init in self.inits:
            if isinstance(init, Number):
                init = Array([init])
            for i in [init]:
                s += f"tf.cast({str(i)}, tf.float32),"
        s += "]"
        return s

    def hazard_rates(self):
        f_repr = self.line("def h(t, state):")
        self.indent()
        f_repr += self.line(f"with tf.name_scope('{self.name}_h') as scope:")
        self.indent()
        f_repr += self.line("state = tf.cast(state, tf.float32)")
        f_repr += self.line("state = tf.unstack(state, axis=-1)")
        f_repr += self.line("state_shape = state[0].shape")
        f_repr += self.line(f"{','.join(self.states)} = state")
        f_repr += self.line("return [")
        self.indent()
        for tx in self.transitions:
            f_repr += self.line(f"tf.broadcast_to({tx.rate}, state_shape),")
        f_repr += self.line("]")
        self.dedent()
        self.dedent()
        self.dedent()
        return f_repr

    def fargs(self):
        if len(self.param) == 0:
            return ""
        str_args = [str(arg) for arg in self.param]
        return ", ".join(str_args) + ","

    def __str__(self):
        s = self.line("")
        s += self.line(f"def {self.name}({self.fargs()} name=None):")
        self.indent()
        s += self.line(
            f"stoichiometry_ = np.array({self.stoichiometry()}, \
dtype=np.float32)"
        )
        s += self.line(f"initial_state_ = tf.stack({self.initial()}, axis=-1)")
        s += self.line("")
        s += self.hazard_rates()
        s += self.line(
            f"return gld.DiscreteTimeStateTransitionModel(transition_rates=h, \
stoichiometry=stoichiometry_, initial_state=initial_state_, \
initial_step={self.initial_step}, time_delta={self.time_delta}, \
num_steps={self.num_steps}, name=name)"
        )
        return s


class Transition(Outputter):
    def __init__(self, src, dest, rate=0.0):
        self.src = src
        self.dest = dest
        self.rate = rate

    def __str__(self):
        s = f"""Transition({self.src}, {self.dest}, {self.rate})"""
        return s

    def __repr__(self):
        return str(self)


class Function(Outputter):
    def __init__(self, name, args):
        self.args = ""
        args = [str(arg) for arg in args]
        if len(args) > 0:
            self.args = ", ".join(args) + ","
        self.fname = name

    def __str__(self):
        if self.fname == "matvec":
            return f"tf.linalg.matvec({self.args})"

        return f"{self.fname}({self.args})"


class BinaryOp(Outputter):
    def __init__(self, lhs, rhs, **kwargs):
        super().__init__(**kwargs)
        self.lhs = lhs
        self.rhs = rhs


class Truediv(BinaryOp):
    def __init__(self, lhs, rhs, **kwargs):
        super().__init__(lhs, rhs, **kwargs)

    def __str__(self):
        return f"tf.math.truediv({self.lhs}, {self.rhs})"


class Mul(BinaryOp):
    def __init__(self, lhs, rhs, **kwargs):
        super().__init__(lhs, rhs, **kwargs)

    def __str__(self):
        return f"tf.math.multiply({self.lhs}, {self.rhs})"


class Matmul(BinaryOp):
    def __init__(self, lhs, rhs, **kwargs):
        super().__init__(lhs, rhs, **kwargs)

    def __str__(self):
        return f"tf.linalg.matmul({self.lhs}, {self.rhs})"


class Matvec(BinaryOp):
    def __init__(self, lhs, rhs, **kwargs):
        super().__init__(lhs, rhs, **kwargs)

    def __str__(self):
        return f"tf.linalg.matvec({self.lhs}, {self.rhs})"


class Add(BinaryOp):
    def __init__(self, lhs, rhs, **kwargs):
        super().__init__(lhs, rhs, **kwargs)

    def __str__(self):
        return f"tf.math.add({self.lhs}, {self.rhs})"


class Sub(BinaryOp):
    def __init__(self, lhs, rhs, **kwargs):
        super().__init__(lhs, rhs, **kwargs)

    def __str__(self):
        return f"tf.math.subtract({self.lhs}, {self.rhs})"


class Pow(BinaryOp):
    def __init__(self, lhs, rhs, **kwargs):
        super().__init__(lhs, rhs, **kwargs)

    def __str__(self):
        return f"tf.math.pow({self.lhs}, {self.rhs})"


class Lt(BinaryOp):
    def __init__(self, lhs, rhs, **kwargs):
        super().__init__(lhs, rhs, **kwargs)

    def __str__(self):
        return f"tf.math.less({self.lhs}, {self.rhs})"


class Gt(BinaryOp):
    def __init__(self, lhs, rhs, **kwargs):
        super().__init__(lhs, rhs, **kwargs)

    def __str__(self):
        return f"tf.math.greater({self.lhs}, {self.rhs})"


class Leq(BinaryOp):
    def __init__(self, lhs, rhs, **kwargs):
        super().__init__(lhs, rhs, **kwargs)

    def __str__(self):
        return f"tf.math.less_equal({self.lhs}, {self.rhs})"


class Geq(BinaryOp):
    def __init__(self, lhs, rhs, **kwargs):
        super().__init__(lhs, rhs, **kwargs)

    def __str__(self):
        return f"tf.math.greater_equal({self.lhs}, {self.rhs})"


class Eq(BinaryOp):
    def __init__(self, lhs, rhs, **kwargs):
        super().__init__(lhs, rhs, **kwargs)

    def __str__(self):
        return f"tf.math.equal({self.lhs}, {self.rhs})"


class Ne(BinaryOp):
    def __init__(self, lhs, rhs, **kwargs):
        super().__init__(lhs, rhs, **kwargs)

    def __str__(self):
        return f"tf.math.not_equal({self.lhs}, {self.rhs})"


class UnaryOp(Outputter):
    def __init__(self, operand, **kwargs):
        super().__init__(**kwargs)
        self.operand = operand


class Neg(UnaryOp):
    def __init__(self, operand, **kwargs):
        super().__init__(operand, **kwargs)

    def __str__(self):
        return f"tf.math.negative({self.operand})"


class Not(UnaryOp):
    def __init__(self, operand, **kwargs):
        super().__init__(operand, **kwargs)

    def __str__(self):
        return f"tf.math.logical_not({self.operand})"


class AssignExternal(Outputter):
    def __init__(self, symbol_name, **kwargs):
        super().__init__(**kwargs)
        self.symbol_name = symbol_name

    def __str__(self):
        return f"{self.symbol_name} = gem_external['{self.symbol_name}']"


class Assign(Outputter):
    def __init__(self, lhs, rhs, **kwargs):
        super().__init__(**kwargs)
        self.lhs = lhs
        self.rhs = rhs

    def __str__(self):
        if isinstance(self.rhs, (Number, Array)):
            rhs_expr = f"tf.convert_to_tensor({self.rhs})"
        else:
            rhs_expr = f"{self.rhs}"
        return f"{self.lhs} = {rhs_expr}"


class StochasticAssign(Outputter):
    def __init__(self, lhs, rhs, is_root_node=False, **kwargs):
        super().__init__(**kwargs)
        self.lhs = lhs
        self.rhs = rhs
        self.is_root_node = is_root_node

    def __str__(self):
        if self.is_root_node is True:
            return f"{self.lhs} = yield Root({self.rhs})"
        else:
            return f"{self.lhs} = yield {self.rhs}"


class STMInstance(Function):
    """Wraps a call to an Epi constructor"""

    def __init__(self, fname, args, value, name):
        super().__init__(fname, args)
        self.value = value
        self.name = name

    def __str__(self):
        s = f"{self.fname}("
        if self.args:
            s += f"{self.args} "
        s += f"name='{self.name}')"
        return s


class IndexExpr(Outputter):
    def __init__(self, expr, index_list):
        self.expr = expr
        self.index_list = index_list

    def __str__(self):
        indices_str = str(Array(self.index_list))
        return f"{self.expr}{indices_str}"


class Array(Outputter):
    def __init__(self, elems):
        self.value = elems

    def __str__(self):
        out_elems = []
        for elem in self.value:
            if isinstance(elem, Number):
                out_elems.append(str(elem.value))
            else:
                out_elems.append(str(elem))
        return f"[{', '.join(out_elems)}]"


class Number(Outputter):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return f"{self.value}"


# Builtin gemlang funcs
class BuiltinGamma(Function):
    def __init__(self, args, value=None, name=None):
        """
        Instantiates a Gamma distribution

        :param args: a list of arguments corresponding to parameters of the
        Gamma distribution [shape, rate]
        """
        self.fname = "tfd.Gamma"
        self.tag = "Gamma"
        self.args = f"concentration={args[0]}, rate={args[1]}"
        if value:
            self.args += f", value={value}"
        if name:
            self.args += f", name='{name}'"


class BuiltinNormal(Function):
    def __init__(self, args, value=None, name=None):
        """
        Instantiates a Normal distribution

        :param args: a list of arguments corresponding to parameters of the
        Normal distribution [mean, variance]
        """
        self.fname = "tfd.Normal"
        self.tag = "Normal"
        self.args = f"loc={args[0]}, scale={args[1]}"
        if value:
            self.args += f", value={value}"
        if name:
            self.args += f", name='{name}'"


class BuiltinState(Function):
    def __init__(self, initial):
        """
        Instantiates a State :param initial: the initial state
        """
        assert isinstance(
            initial, (str, float, int)
        ), "Value must be str, float, or int."
        self.initial = initial


class BuiltinExp(Function):
    def __init__(self, arg):
        super().__init__("tf.math.exp", arg)


builtins_ = {
    "State": BuiltinState,
    "Gamma": BuiltinGamma,
    "Normal": BuiltinNormal,
    "exp": BuiltinExp,
}
