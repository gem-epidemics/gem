##############################################################################
# Copyright 2019 Chris Jewell <c.jewell@lancaster.ac.uk>                     #
#                                                                            #
# Permission is hereby granted, free of charge, to any person obtaining a    #
# copy of this software and associated documentation files (the "Software"), #
# to deal in the Software without restriction, including without limitation  #
# the rights to use, copy, modify, merge, publish, distribute, sublicense,   #
# and/or sell copies of the Software, and to permit persons to whom the      #
# Software is furnished to do so, subject to the following conditions:       #
#                                                                            #
# The above copyright notice and this permission notice shall be included    #
# in all copies or substantial portions of the Software.                     #
#                                                                            #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR #
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   #
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    #
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER #
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    #
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        #
# DEALINGS IN THE SOFTWARE.                                                  #
##############################################################################

# Utility functions for gemlang

from lark import Transformer, v_args


@v_args(inline=True)
class Expr2Infix(Transformer):
    """Translate an AST expression to infix notation"""

    # pylint: disable=missing-function-docstring,no-self-use,unused-argument
    # pylint: disable=invalid-name

    def idref(self, s):
        return str(s)

    def number(self, n):
        return n

    def add(self, lhs, rhs):
        return "({lhs} + {rhs})".format(lhs=lhs, rhs=rhs)

    def sub(self, lhs, rhs):
        return "({lhs} - {rhs})".format(lhs=lhs, rhs=rhs)

    def mul(self, lhs, rhs):
        return "({lhs} * {rhs})".format(lhs=lhs, rhs=rhs)

    def matmul(self, lhs, rhs):
        return "({lhs} @ {rhs})".format(lhs=lhs, rhs=rhs)

    def truediv(self, lhs, rhs):
        return "({lhs} / {rhs})".format(lhs=lhs, rhs=rhs)

    def pow(self, lhs, rhs):
        return "({lhs}^{rhs})".format(lhs=lhs, rhs=rhs)

    def lt(self, lhs, rhs):
        return "({lhs} < {rhs})".format(lhs=lhs, rhs=rhs)

    def gt(self, lhs, rhs):
        return "({lhs} > {rhs})".format(lhs=lhs, rhs=rhs)

    def le(self, lhs, rhs):
        return "({lhs} <= {rhs})".format(lhs=lhs, rhs=rhs)

    def ge(self, lhs, rhs):
        return "({lhs} >= {rhs})".format(lhs=lhs, rhs=rhs)

    def eq(self, lhs, rhs):
        return "({lhs} == {rhs})".format(lhs=lhs, rhs=rhs)

    def ne(self, lhs, rhs):
        return "({lhs} != {rhs})".format(lhs=lhs, rhs=rhs)

    def neg(self, x):
        return "(-{x})".format(x=x)

    def not_(self, x):
        return "(!{x})".format(x=x)

    def assign(self, symbol, expression):
        return "{s} = {ex}".format(s=symbol, ex=expression)

    def call(self, id, fargs):
        s = "{f}(".format(f=id)
        fargs = list(fargs)
        if len(fargs) > 0:
            s += str(fargs.pop(0))
        for farg in fargs:
            s += ", {}".format(str(farg))
        return "{})".format(s)

    def arglist(self, *fargs):
        return fargs


expr2infix_ = Expr2Infix()


def expr2infix(x):  # pylint: disable=invalid-name
    """Translate a parse tree expression to infix"""
    return expr2infix_.transform(x)
