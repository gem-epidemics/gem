##############################################################################
# Copyright 2019 Chris Jewell <c.jewell@lancaster.ac.uk>                     #
#                                                                            #
# Permission is hereby granted, free of charge, to any person obtaining a    #
# copy of this software and associated documentation files (the "Software"), #
# to deal in the Software without restriction, including without limitation  #
# the rights to use, copy, modify, merge, publish, distribute, sublicense,   #
# and/or sell copies of the Software, and to permit persons to whom the      #
# Software is furnished to do so, subject to the following conditions:       #
#                                                                            #
# The above copyright notice and this permission notice shall be included    #
# in all copies or substantial portions of the Software.                     #
#                                                                            #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR #
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   #
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    #
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER #
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    #
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        #
# DEALINGS IN THE SOFTWARE.                                                  #
##############################################################################

"""The main GEM Python interface"""

import time
from collections import namedtuple

import numpy as np
import tensorflow as tf
import tensorflow_probability as tfp

from gem.gemlang import GEMParser
from gem.gemlang.model_generator import CodeGenerator
from gem.gemlang.semantics import TypeResolver
from gem.gemlang.semantics.annotate_root_random_vars import (
    AnnotateRootDAGNodes,
)
from gem.gemlang.semantics.symbol_declare import SymbolDeclarer
from gem.gemlang.semantics.symbol_resolve import (
    SymbolResolver,
    get_random_vars,
)
from gem.gemlang.semantics.symbol_table import SymbolTable


def as_numpy_dict(dictionary):  # Todo move to a utils module
    """
    Converts values in a dictionary to numpy objects.

    :param dictionary a dict where values are array-like objects. :return a
    dict where values are np.array objects.
    """
    ret_val = {}
    for key, val in dictionary.items():
        if hasattr(val, "__iter__"):
            val = np.array(val)
        ret_val[key] = val
    return ret_val


def _tf2numpy(struct_tuple):
    return struct_tuple.__class__(
        **{k: v.numpy() for k, v in struct_tuple._asdict().items()}
    )


class GEM:
    """Represents a GEM model."""

    def __init__(self, gemprog, const_data=None):
        """
        Builds a GEM model given a GEM program and data to attach.

        The actual model function is attached to the GEM instance as the method
        GEM.model_impl.

        :param gemprog: a GEM program
        :type gemprog: str
        :param const_data: dictionary of constant (i.e. covariate) data to
                           attach to the model.
        :type const_data: dict
        :returns: a module object implementing the model.
        """

        self.__symtab = None
        self.ast = None
        self.__pyprog = "# Undefined"
        self.model_impl = lambda *args, **kwargs: {}
        self.gem_external = {} if const_data is None else const_data
        self.__parse__(gemprog)
        # self.log_prob_fn = ed.make_log_joint_fn(self.model_impl)

    @property
    def pyprog(self):
        """Returns the serialized Python representation of the model."""
        return self.__pyprog

    @property
    def symtab(self):
        """Returns the model symbol table."""
        return self.__symtab

    @property
    def random_variables(self):
        """Returns a dictionary of random variables declared in the model."""
        rv_names = get_random_vars(self.__symtab)
        return {k: v for k, v in self.model_impl().items() if k in rv_names}

    def __getitem__(self, random_variable):
        var_index = self.model_impl.event_shape._field_name_to_index
        return self.model_impl.submodules[var_index[random_variable]]

    def __create_symbol_table(self):
        symbol_table = SymbolTable()
        symbol_table.declare_extern(self.gem_external)
        self.__symtab = symbol_table

    def __parse__(self, gemprog):
        try:
            ast = GEMParser(gemprog)
            self.__create_symbol_table()
            SymbolDeclarer().visit(ast, self.__symtab)
            SymbolResolver().visit(ast, self.__symtab)
            TypeResolver().visit(ast, self.__symtab)
            AnnotateRootDAGNodes().visit(ast, self.__symtab)
            self.__pyprog = CodeGenerator().generate(
                ast, self.__symtab, self.gem_external
            )

            self.ast = ast

        except Exception as exception:
            print(f"An error occurred during model compilation: {exception}")
            raise exception

        try:
            exec(
                self.__pyprog, self.__dict__  # pylint: disable=exec-used
            )  # Adds dynamically built self.model_impl()
        except Exception as exception:
            print("An error occurred executing the compiled model")
            print(self.__pyprog)
            print(self.__symtab)
            raise exception

    @property
    def log_prob_fn(self):
        return self.model_impl.log_prob

    def log_prob(self, *args, **kwargs):
        """
        Returns the log posterior of the model evaluated at kwargs.

        :param kwargs: a dictionary of parameter values at which to evaluate
                       the log posterior.
        :returns: the value of the log posterior
        """
        return self.log_prob_fn(*args, **kwargs).numpy()

    def sample(self, n_samples=1, condition_vars=None, seed=None):
        """
        Draws samples from the model prior distribution.

        :param n_samples: the number of independent samples to draw.
        :param condition_vars: a dict of random variable values to condition on
                               {'varname': value}

        :return a dict of realisations of random variables in the model
        """
        condition_vars = {} if condition_vars is None else condition_vars
        sample_shape = () if n_samples == 1 else (n_samples,)
        draws = self.model_impl.sample(
            sample_shape=sample_shape, **condition_vars, seed=seed
        )

        return _tf2numpy(draws)

    def fit(
        self,
        observed,
        n_samples,
        init=None,
        num_burnin_steps=0,  # Todo move MCMC implementation to external func
    ):
        """
        Fits a model to observed data

        :param observed: a dict of observed values, with keys denoting
                         variables within the GEM model.
        :param n_samples: number of samples to draw from the posterior
                          distribution
        :param init: dict of initialisation values
        :param burnin: the number of burnin samples to take
        :param clip_burnin: whether to clip the burnin or not

        :returns: a dict of posterior samples for each unobserved variable

        """

        def tlp_fn(*args, **kwargs):
            pinned_model = self.model_impl.experimental_pin(**observed)
            return pinned_model.unnormalized_log_prob(*args, **kwargs)

        kernel = tfp.mcmc.DualAveragingStepSizeAdaptation(
            inner_kernel=tfp.mcmc.HamiltonianMonteCarlo(
                target_log_prob_fn=tlp_fn,
                num_leapfrog_steps=16,
                step_size=0.1,
            ),
            num_adaptation_steps=int(num_burnin_steps * 0.8),
        )

        StateTuple = namedtuple("StateTuple", init)
        current_state = StateTuple(**init)

        @tf.function(jit_compile=True)
        def sample():
            return tfp.mcmc.sample_chain(
                num_results=n_samples,
                current_state=current_state,
                kernel=kernel,
            )

        print("Sampling...", flush=True)
        start = time.perf_counter()
        samples, results = sample()
        end = time.perf_counter()
        print(f"Done in {end - start} seconds", flush=True)
        return _tf2numpy(samples), results
