##############################################################################
# Copyright 2019 Chris Jewell <c.jewell@lancaster.ac.uk>                     #
#                                                                            #
# Permission is hereby granted, free of charge, to any person obtaining a    #
# copy of this software and associated documentation files (the "Software"), #
# to deal in the Software without restriction, including without limitation  #
# the rights to use, copy, modify, merge, publish, distribute, sublicense,   #
# and/or sell copies of the Software, and to permit persons to whom the      #
# Software is furnished to do so, subject to the following conditions:       #
#                                                                            #
# The above copyright notice and this permission notice shall be included    #
# in all copies or substantial portions of the Software.                     #
#                                                                            #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR #
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   #
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    #
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER #
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    #
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        #
# DEALINGS IN THE SOFTWARE.                                                  #
##############################################################################

"""Timeseries plotting functions"""

import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from cycler import cycler
from gemlib.util import compute_state

__all__ = ["plot_state_timeseries"]


def plot_state_timeseries(
    epi,
    event_list,
    state_labels,
    initial_time=0.0,
    sharex=True,
    sharey=True,
    landscape=True,
    show=True,
):
    """
    Plots an epidemic timeseries.

    :param epi: an instance of base type `tfd.Distribution` describing an
                epidemic.
    :param event_list: an event list as returned by a call to `epi.sample()`.
    :param state_labels: python list of state labels in the same order as
                         defined in the model e.g. ("S","I","R").
    :param initial_time: python float giving starting value for timeseries on
                         x-axis.
    :param sharex: if `True` x-axis will be shared among all subplots.
    :param sharey: if `True` y-axis will be shared among all subplots.
    :param landscape: if `True` subplots are stacked horizontally otherwise
                      vertically.
    :param show: if `True` call `plt.show()`.
    """

    state_timeseries = compute_state(
        epi.initial_state, event_list, epi.stoichiometry
    )

    if (
        epi.event_shape[0] == 1
    ):  # one meta population with all states on single plot
        state_timeseries = tf.squeeze(state_timeseries, axis=-3)
        color_cycle = cycler("color", ["b", "r", "g", "c", "m", "y", "k"])
        for s_num, (state, color) in enumerate(zip(state_labels, color_cycle)):
            plt.step(
                np.arange(state_timeseries.shape[-2]),
                state_timeseries[..., s_num],
                where="post",
                label=state,
                color=color["color"],
            )
        plt.legend()
    else:  # subplot for each meta population in a given state
        if landscape is True:
            _, ax = plt.subplots(
                nrows=1, ncols=len(state_labels), sharey=sharey, sharex=sharex
            )
        else:
            _, ax = plt.subplots(
                nrows=len(state_labels), ncols=1, sharey=sharey, sharex=sharex
            )
        x = np.tile(
            np.arange(initial_time, state_timeseries.shape[-2] + initial_time),
            (state_timeseries.shape[-3], 1),
        )
        for i in np.arange(len(state_labels)):
            y = state_timeseries[..., i]
            ax[i].step(
                tf.transpose(x),
                tf.transpose(y),
                where="post",
            )
            ax[i].set_title(state_labels[i])

    if show:
        plt.show()
