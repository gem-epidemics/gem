##############################################################################
# Copyright 2019 Chris Jewell <c.jewell@lancaster.ac.uk>                     #
#                                                                            #
# Permission is hereby granted, free of charge, to any person obtaining a    #
# copy of this software and associated documentation files (the "Software"), #
# to deal in the Software without restriction, including without limitation  #
# the rights to use, copy, modify, merge, publish, distribute, sublicense,   #
# and/or sell copies of the Software, and to permit persons to whom the      #
# Software is furnished to do so, subject to the following conditions:       #
#                                                                            #
# The above copyright notice and this permission notice shall be included    #
# in all copies or substantial portions of the Software.                     #
#                                                                            #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR #
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   #
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    #
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER #
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    #
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        #
# DEALINGS IN THE SOFTWARE.                                                  #
##############################################################################

"""A function to produce an automatic traceplot"""

import matplotlib.pyplot as plt
import seaborn as sns
import statsmodels.graphics.tsaplots


def traceplot(posterior, num_burnin_steps=0, show=True):
    """
    Plots traceplot, kernel density, and acf for a GEM posterior.

    :param posterior: a `namedtuple` of posterior draws for parameters
    :param show: if `True` call `plt.show()`
    """
    posterior_dict = posterior._asdict()

    fig, ax = plt.subplots(len(posterior_dict), 3)
    for i, var in enumerate(posterior_dict.items()):
        ax_i = ax if len(posterior_dict) == 1 else ax[i]
        ax_i[0].plot(var[1][num_burnin_steps:], "-")
        sns.kdeplot(var[1][num_burnin_steps:], ax=ax_i[1])
        statsmodels.graphics.tsaplots.plot_acf(
            var[1][num_burnin_steps:], ax=ax_i[2]
        )

    if show is True:
        plt.show()
    return fig, ax
