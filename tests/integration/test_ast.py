##############################################################################
# Copyright 2019 Chris Jewell <c.jewell@lancaster.ac.uk>                     #
#                                                                            #
# Permission is hereby granted, free of charge, to any person obtaining a    #
# copy of this software and associated documentation files (the "Software"), #
# to deal in the Software without restriction, including without limitation  #
# the rights to use, copy, modify, merge, publish, distribute, sublicense,   #
# and/or sell copies of the Software, and to permit persons to whom the      #
# Software is furnished to do so, subject to the following conditions:       #
#                                                                            #
# The above copyright notice and this permission notice shall be included    #
# in all copies or substantial portions of the Software.                     #
#                                                                            #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR #
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   #
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    #
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER #
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    #
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        #
# DEALINGS IN THE SOFTWARE.                                                  #
##############################################################################

"""Test AST components"""

import inspect
import os
import unittest

from lark import Lark

from gem import gemlang
from gem.gemlang.ast.ast_expression import AddExpr, Number, SubExpr
from gem.gemlang.ast.utils import serialize
from gem.gemlang.ast_walker import ASTWalker
from gem.gemlang.parse_gemlang import ParseTree2AST
from gem.gemlang.util import expr2infix


@unittest.skip("Known bug in expr2infix")
class TestExpr2Infix(unittest.TestCase):
    def setUp(self):
        glpath = os.path.dirname(inspect.getfile(gemlang))
        with open(os.path.join(glpath, "gem_grammar.cfgr"), "r") as f:
            grammar = f.read()
        self.gem_lp = Lark(grammar, start="expr", propagate_positions=True)

        with open(os.path.join(glpath, "ast/ast_grammar.cfgr"), "r") as f:
            grammar = f.read()
        self.ast_lp = Lark(grammar, start="expr")

    def test_ast2infix(self):
        prog = "beta * sizeof(S) * sizeof(I) + 3. / (3. - 1.)"
        gem_tree = self.gem_lp.parse(prog)
        ast = ParseTree2AST().transform(gem_tree)
        ast_tree = self.ast_lp.parse(serialize(ast))
        infix = expr2infix(ast_tree)
        self.assertEqual(
            "(((beta * sizeof(S)) * sizeof(I)) + (3.0 / (3.0 - 1.0)))", infix
        )


class TestASTVisitor(unittest.TestCase):
    def setUp(self):
        class Walker(ASTWalker):
            def walk(self, node):
                return self._walk(node)

            def onEnter_AddExpr(self, node):
                pass

            def onEnter_SubExpr(self, node):
                pass

            def onExit_AddExpr(self, node):
                pass

            def onExit_SubExpr(self, node):
                pass

        self.visitor = Walker()

    def test_addsub(self):
        from gem.gemlang.ast.ast_expression import AddExpr, Number, SubExpr

        x = Number(1.0)
        y = Number(3.5)
        z = Number(2.3)
        addnode = AddExpr(x, y)
        subnode = SubExpr(z, addnode)

        result = self.visitor.walk(subnode)
        self.assertEqual(
            "(SubExpr (Number 2.3) (AddExpr (Number 1.0) (Number 3.5)))",
            serialize(result),
        )


class TestASTWalkerTransform(unittest.TestCase):
    def setUp(self):
        class Walker(ASTWalker):
            def onExit_AddExpr(self, node):
                return Number(node.children[0].value + node.children[1].value)

        self.visitor = Walker()

    def test_addsub(self):
        x = Number(1.0)
        y = Number(3.5)
        z = Number(2.3)
        addnode = AddExpr(x, y)
        subnode = SubExpr(z, addnode)

        subnode = self.visitor._walk(subnode)
        self.assertEqual(
            "(SubExpr (Number 2.3) (Number 4.5))", serialize(subnode)
        )
