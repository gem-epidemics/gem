##############################################################################
# Copyright 2019 Chris Jewell <c.jewell@lancaster.ac.uk>                     #
#                                                                            #
# Permission is hereby granted, free of charge, to any person obtaining a    #
# copy of this software and associated documentation files (the "Software"), #
# to deal in the Software without restriction, including without limitation  #
# the rights to use, copy, modify, merge, publish, distribute, sublicense,   #
# and/or sell copies of the Software, and to permit persons to whom the      #
# Software is furnished to do so, subject to the following conditions:       #
#                                                                            #
# The above copyright notice and this permission notice shall be included    #
# in all copies or substantial portions of the Software.                     #
#                                                                            #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR #
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   #
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    #
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER #
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    #
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        #
# DEALINGS IN THE SOFTWARE.                                                  #
##############################################################################

"""gemlang tests"""

import inspect
import os
import unittest

from lark import Lark

from gem import GEM, gemlang
from gem.gemlang.ast.utils import serialize
from gem.gemlang.parse_gemlang import ParseTree2AST


class TestGemlang(unittest.TestCase):
    """General gemlang integration tests"""

    def setUp(self):
        glpath = os.path.dirname(inspect.getfile(gemlang))
        with open(os.path.join(glpath, "gem_grammar.cfgr"), "r") as f:
            grammar = f.read()
        self.parser = Lark(
            grammar,
            start="gem",
            ambiguity="explicit",
            propagate_positions=True,
        )

    def test_ambiguity(self):
        """Tests if the parser raises ambiguity errors"""
        prog = """
        popsize = 100
        epsilon ~ Gamma(1.0, 1.0)
        beta ~ Gamma(1.0, 1.0)
        gamma ~ Gamma(1., .1)
        I0 ~ Multinomial(n=1, size=popsize)

        Epidemic<type="ILM", time_origin=0., time_step=1.> MyModel() {

            S = State(init=!I0)
            I = State(init=I0)
            R = State(init=0)

            siRate = epsilon + beta * K * I

            [S -> I] = siRate
            [I -> R] = gamma
        }
        epi = MyModel()
        """
        tree = self.parser.parse(prog)
        if tree.data == "_ambig":
            self.fail(
                "Parser received {} ambiguous trees.".format(
                    len(tree.children)
                )
            )


class TestExpressionParse(unittest.TestCase):
    """Tests to ensure deterministic ASTs"""

    def setUp(self):
        glpath = os.path.dirname(inspect.getfile(gemlang))
        with open(os.path.join(glpath, "gem_grammar.cfgr"), "r") as f:
            grammar = f.read()

        self.parser = Lark(
            grammar, start="expr", parser="earley", propagate_positions=True
        )

    def test_arith(self):
        """Complex expression"""
        program = """beta * pow(I, 3.) * S / (3.0 - 2.) * 5.0"""
        tree = self.parser.parse(program)
        parser = ParseTree2AST()
        ast = parser.transform(tree)
        self.assertEqual(
            "(MulExpr (MulExpr (MulExpr (IdRef beta) (Call (IdRef pow) (ArgList \
(IdRef I) (Float 3.0)))) (TruedivExpr (IdRef S) (SubExpr (Float 3.0) \
(Float 2.0)))) (Float 5.0))",
            serialize(ast),
        )


class TestDeclParse(unittest.TestCase):
    """Expressions"""

    def setUp(self):
        glpath = os.path.dirname(inspect.getfile(gemlang))
        with open(os.path.join(glpath, "gem_grammar.cfgr"), "r") as f:
            grammar = f.read()

        self.parser = Lark(grammar, start="gem", propagate_positions=True)

    def test_1d_array(self):
        """Compound numeric"""
        stmt = """[3, 3, 3]"""
        self.parser.parse(stmt)

    def test_2d_array(self):
        """2-d array"""
        stmt = """[[1,2],[3,4],[5,6]]"""
        tree = self.parser.parse(stmt)
        parser = ParseTree2AST()
        parser.transform(tree)

    def test_varassign(self):
        """Assign"""
        stmt = """x = 1 + 3"""
        tree = self.parser.parse(stmt)
        sp = ParseTree2AST()
        ast = sp.transform(tree)
        self.assertEqual(
            "(GEMProgram (AssignExpr (IdRef x) (AddExpr (Integer 1) \
(Integer 3))))",
            serialize(ast),
        )

    def test_stochassign(self):
        """Stochastic assign"""
        stmt = """beta ~ Gamma(0.1, 0.1)"""
        tree = self.parser.parse(stmt)
        sp = ParseTree2AST()
        ast = sp.transform(tree)
        self.assertEqual(
            "(GEMProgram (StochasticAssignExpr (IdRef beta) (Call (IdRef Gamma) \
(ArgList (Float 0.1) (Float 0.1)))))",
            serialize(ast),
        )

    def test_transition_assign(self):
        """ "Transition assign"""
        stmt = "[S -> I] = gamma"
        tree = self.parser.parse(stmt)
        sp = ParseTree2AST()
        ast = sp.transform(tree)
        self.assertEqual(
            "(GEMProgram (AssignTransition (TransitionRef (IdRef S) (IdRef I)) \
(IdRef gamma)))",
            serialize(ast),
        )

    def test_epidecl(self):
        stmt = """
        Epidemic<type="ILM",
                 time_origin=0.,
                 time_step=1.> myModel (initial_population_size) {

                S = State(init=!I0)
                I = State(init=I0)
                R = State(init=0)

                siRate = epsilon + beta * sizeof(I)

                [S -> I] = siRate
                [I -> R] = gamma
        }
        epi ~ myModel(1000)
        """

        tree = self.parser.parse(stmt)
        parser = ParseTree2AST()
        ast = parser.transform(tree)
        self.assertEqual(
            """(GEMProgram (EpiDecl myModel (ArgList (KwArg (KwRef type) \
(String "ILM")) (KwArg (KwRef time_origin) (Float 0.0)) (KwArg \
(KwRef time_step) (Float 1.0))) (FArgList (FArg initial_population_size))\
 (Block (AssignExpr \
(IdRef S) (Call (IdRef State) (ArgList (KwArg (KwRef init) (Not_Expr (IdRef \
I0)))))) (AssignExpr (IdRef I) (Call (IdRef State) (ArgList (KwArg (KwRef \
init)\
 (IdRef I0))))) (AssignExpr (IdRef R) (Call (IdRef State) (ArgList (KwArg \
(KwRef init) (Integer 0))))) (AssignExpr (IdRef siRate) (AddExpr (IdRef \
epsilon) (MulExpr (IdRef beta) (Call (IdRef sizeof) (ArgList (IdRef I))\
)))) (AssignTransition (TransitionRef (IdRef S) (IdRef I)) (IdRef \
siRate)) (AssignTransition (TransitionRef (IdRef I) (IdRef R)) (IdRef \
gamma)))) (StochasticAssignExpr (IdRef epi) (Call (IdRef myModel) \
(ArgList (Integer 1000)))))""",
            serialize(ast),
        )


class TestGEMLang(unittest.TestCase):
    def setUp(self):
        glpath = os.path.dirname(inspect.getfile(gemlang))
        with open(os.path.join(glpath, "gem_grammar.cfgr"), "r") as f:
            grammar = f.read()
        self.parser = Lark(
            grammar, start="gem", parser="earley", propagate_positions=True
        )

    def test_program(self):
        """Test a full program"""
        program = """
        popsize = 100
        epsilon ~ Gamma(1.0, 1.0)
        beta ~ Gamma(1.0, 1.0)
        gamma ~ Gamma(1., .1)
        I0 ~ Multinomial(n=1, size=popsize)

        Epidemic <type="ILM", time_origin=0., time_step=1.> MyModel(lemon) {

            S = State(init=1-I0)
            I = State(init=I0)
            R = State(init=0)

            siRate = epsilon + beta * sizeof(I)

            [S -> I] = siRate
            [I -> R] = gamma
        }
        x ~ MyModel()
        """

        tree = self.parser.parse(program)
        parser = ParseTree2AST()
        ast = parser.transform(tree)
        self.assertEqual(
            """\
(GEMProgram (AssignExpr (IdRef popsize) (Integer 100)) (StochasticAssignExpr \
(IdRef epsilon) (Call (IdRef Gamma) (ArgList (Float 1.0) (Float 1.0)))) \
(StochasticAssignExpr (IdRef beta) (Call (IdRef Gamma) (ArgList (Float 1.0) \
(Float 1.0)))) (StochasticAssignExpr (IdRef gamma) (Call (IdRef Gamma) \
(ArgList\
 (Float 1.0) (Float 0.1)))) (StochasticAssignExpr (IdRef I0) (Call (IdRef \
Multinomial) (ArgList (KwArg (KwRef n) (Integer 1)) (KwArg (KwRef size) \
(IdRef \
popsize))))) (EpiDecl MyModel (ArgList (KwArg (KwRef type) (String "ILM")) \
(KwArg (KwRef time_origin) (Float 0.0)) (KwArg (KwRef time_step) \
(Float 1.0))) \
(FArgList (FArg lemon)) (Block (AssignExpr (IdRef S) (Call (IdRef State) \
(ArgList (KwArg (KwRef init) (SubExpr (Integer 1) (IdRef I0)))))) (AssignExpr \
(IdRef I) (Call (IdRef State) (ArgList (KwArg (KwRef init) (IdRef I0))))) \
(AssignExpr (IdRef R) (Call (IdRef State) (ArgList (KwArg (KwRef init) \
(Integer \
0))))) (AssignExpr (IdRef siRate) (AddExpr (IdRef epsilon) (MulExpr (IdRef \
beta) (Call (IdRef sizeof) (ArgList (IdRef I)))))) (AssignTransition \
(TransitionRef (IdRef S) (IdRef I)) (IdRef siRate)) (AssignTransition \
(TransitionRef (IdRef I) (IdRef R)) (IdRef gamma)))) (StochasticAssignExpr \
(IdRef x) (Call (IdRef MyModel) (ArgList))))\
""",
            serialize(ast),
        )

    def test_redeclaration(self):
        program = """
        epsilon ~ Gamma(1.0, 1.0)
        epsilon ~ Gamma (2.0, 1.0)
        """
        self.assertRaises(SyntaxError, GEM, program)

    def test_data_scalar(self):  # pylint: disable=no-self-use
        """Do scalars make it through?"""
        program = """
        a = Scalar()
        """
        GEM(program, const_data={"a": 2})

    def test_rv_observation(self):
        """Ensure conditioned variable is passed through okay"""
        program = """
        epsilon ~ Gamma(1.0, 1.0)
        """
        model = GEM(program)
        res = model.sample(1, condition_vars={"epsilon": [1.0]})
        self.assertEqual(1.0, res.epsilon)
