##############################################################################
# Copyright 2019 Chris Jewell <c.jewell@lancaster.ac.uk>                     #
#                                                                            #
# Permission is hereby granted, free of charge, to any person obtaining a    #
# copy of this software and associated documentation files (the "Software"), #
# to deal in the Software without restriction, including without limitation  #
# the rights to use, copy, modify, merge, publish, distribute, sublicense,   #
# and/or sell copies of the Software, and to permit persons to whom the      #
# Software is furnished to do so, subject to the following conditions:       #
#                                                                            #
# The above copyright notice and this permission notice shall be included    #
# in all copies or substantial portions of the Software.                     #
#                                                                            #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR #
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   #
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    #
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER #
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    #
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        #
# DEALINGS IN THE SOFTWARE.                                                  #
##############################################################################

"""Interface tests"""

import unittest

from gem import GEM


class SimpleTfInterpret(unittest.TestCase):
    """Can constants be added"""

    def test_tf_expr(self):
        prog = "1 + 2\n"
        self.assertTrue(GEM(prog))

    def test_decl_assign(self):
        """Simple model, can we sample?"""
        prog = """
        r = 10.
        pi = 3.14157
        rv ~ Gamma(0.1, 0.1)
        result = pi * r * rv
        """
        model = GEM(prog)
        self.assertTrue(model.sample())

    def test_varnames(self):
        """Ensure variable names are present in sample output"""

        prog = """
                beta ~ Gamma(1.0, 1000.0)
                nu ~ Gamma(14.0, 100.0)
                gamma ~ Gamma(14.0, 100.0)

                Epidemic<type="DiscreteTime",
                         time_origin=0.,
                         time_step=0.> MyEpidemic () {

                    S = State(init=999)
                    E = State(init=0)
                    I = State(init=1)
                    R = State(init=0)

                    [S -> E] = beta * I
                    [E -> I] = nu
                    [I -> R] = gamma
                }

                epi ~ MyEpidemic()
                """

        model = GEM(prog)
        self.assertTrue(model.sample())
        res = model.sample()
        self.assertEqual(set(("beta", "nu", "gamma", "epi")), set(res._fields))
