##############################################################################
# Copyright 2019 Chris Jewell <c.jewell@lancaster.ac.uk>                     #
#                                                                            #
# Permission is hereby granted, free of charge, to any person obtaining a    #
# copy of this software and associated documentation files (the "Software"), #
# to deal in the Software without restriction, including without limitation  #
# the rights to use, copy, modify, merge, publish, distribute, sublicense,   #
# and/or sell copies of the Software, and to permit persons to whom the      #
# Software is furnished to do so, subject to the following conditions:       #
#                                                                            #
# The above copyright notice and this permission notice shall be included    #
# in all copies or substantial portions of the Software.                     #
#                                                                            #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR #
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   #
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    #
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER #
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    #
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        #
# DEALINGS IN THE SOFTWARE.                                                  #
##############################################################################

"""Test gemlang interpreter"""

import unittest

from gem.gemlang.ast.ast_base import GEMProgram
from gem.gemlang.parse_gemlang import GEMParser
from gem.gemlang.semantics.symbol_declare import SymbolDeclarer
from gem.gemlang.semantics.symbol_resolve import SymbolResolver
from gem.gemlang.semantics.symbol_table import SymbolTable


class TestInterpreter(unittest.TestCase):
    """Test declare and resolve for scoped symbols"""

    def test_gem_interpreter(self):
        """Interesting epidemic with parameterised epidemic"""

        model_str = """
        beta = 0.0002
        gamma = 0.14

        Epidemic <type="ODE", time_origin=0.> MyEpi (color) {

            S = State(init=999)
            I = State(init=1)
            R = State(init=0)

            [S -> I] = beta * S * I
            [I -> R] = gamma * I
        }

        # Instantiate
        myEpi ~ MyEpi(color="yellow")
        """

        ast = GEMParser(model_str)
        self.assertIsInstance(ast, GEMProgram)
        declarer = SymbolDeclarer()
        symtab = SymbolTable()
        declarer.visit(ast, symtab)
        resolver = SymbolResolver()
        resolver.visit(ast, symtab)
