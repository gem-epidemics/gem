##############################################################################
# Copyright 2019 Chris Jewell <c.jewell@lancaster.ac.uk>                     #
#                                                                            #
# Permission is hereby granted, free of charge, to any person obtaining a    #
# copy of this software and associated documentation files (the "Software"), #
# to deal in the Software without restriction, including without limitation  #
# the rights to use, copy, modify, merge, publish, distribute, sublicense,   #
# and/or sell copies of the Software, and to permit persons to whom the      #
# Software is furnished to do so, subject to the following conditions:       #
#                                                                            #
# The above copyright notice and this permission notice shall be included    #
# in all copies or substantial portions of the Software.                     #
#                                                                            #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR #
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   #
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    #
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER #
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    #
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        #
# DEALINGS IN THE SOFTWARE.                                                  #
##############################################################################

"""Integration tests for statistical inference."""

import os
import pickle as pkl

import numpy as np

from gem import GEM


class TestLogProb:
    """Tests behaviour of log_prob"""

    def test_univariate_free(self):
        """Returns known log prob"""

        prog = """
        epsilon ~ Gamma(1.0, 1.0)
        """

        model = GEM(prog)
        res = model.log_prob(epsilon=1.0)
        np.testing.assert_almost_equal(res, -1.0, decimal=5)

    def test_bivariate_free(self):
        """When sampled, epsilon should have complex shape"""
        prog = """
        epsilon ~ Gamma([1.0, 2.0], 1.0)
        """
        model = GEM(prog)
        res = model.sample()
        np.testing.assert_equal(res.epsilon.shape, (2,))

    def test_nvariate_free(self):
        """When sampled, epsilon should have complex shape"""
        prog = """
        epsilon ~ Gamma([[1.0, 2.0], [3.0, 4.0]], 1.0)
        """
        model = GEM(prog)
        res = model.sample()
        assert res.epsilon.shape == (2, 2)

    def test_vector(self):
        """Tests uninitialized distribution with a vector
        i.e. tests sum of log probs."""
        prog = """
        alpha = Vector()
        epsilon ~ Gamma(alpha, 1.0)
        """
        model = GEM(prog, const_data={"alpha": [1.0, 1.0]})
        res = model.log_prob(epsilon=[1.0, 1.0])
        np.testing.assert_almost_equal(res, -2.0, decimal=6)

    def test_linear_model(self):
        """
        Test a linear model. y = 1.4 + 0.5x + epsilon epsilon ~ Normal(0, 0.01)
        """
        prog = """
        x = Vector()
        alpha ~ Normal(0.0, 1000.0)
        beta ~ Normal(0.0, 1000.0)
        sigma ~ Gamma(1.0, 1.0)
        y ~ Normal(alpha + x, sigma)
        """

        covariate = np.array(
            [
                0.74067508,
                0.49826769,
                0.62670851,
                0.7532123,
                0.79912892,
                0.40918108,
                0.3797415,
                0.95314183,
                0.53511648,
                0.67395316,
            ],
            dtype=np.float32,
        )
        response = np.array(
            [
                1.77808982,
                1.65336958,
                1.69995271,
                1.77873403,
                1.79680289,
                1.5820221,
                1.58739755,
                1.86379043,
                1.66634023,
                1.74729067,
            ],
            dtype=np.float32,
        )
        model = GEM(prog, const_data={"x": covariate})
        res = model.log_prob(alpha=1.4, beta=0.5, sigma=0.01, y=response)
        np.testing.assert_almost_equal(res, -5515.4214, decimal=4)

    def test_epidemic_simple(self):  # pylint: disable=no-self-use
        """Test a simple epidemic model."""
        prog = """
        beta = 0.002
        gamma = 0.14

        Epidemic MyEpidemic() {
            S = State(init=999)
            I = State(init=1)
            R = State(init=0)

           [S -> I] = beta * I
           [I -> R] = gamma
        }
        epi ~ MyEpidemic()
        """
        model = GEM(prog)
        sim = model.sample()
        model.log_prob(**sim._asdict())

    def test_epidemic_model(self):  # pylint: disable=no-self-use
        """Test a properly parameterised epidemic model."""
        prog = """
        beta ~ Gamma(1.0, 1.0)
        gamma ~ Gamma(1.0, 1.0)

        Epidemic MyEpidemic() {
            S = State(init=999)
            I = State(init=1)
            R = State(init=0)

            [S -> I] = beta * I
            [I -> R] = gamma
        }
        epi ~ MyEpidemic()
        """
        model = GEM(prog)
        sim = model.sample()
        model.log_prob(**sim._asdict())


class TestInference:
    """Test inference mechanism"""

    def is_within_credibility_interval(self, true_vals, draws, alpha=0.05):
        """Test if `true_vals` lies within the `1-alpha` CrI of `draws`"""

        draws = list(draws._asdict().values())
        quantiles = np.quantile(a=draws, q=[alpha, 1.0 - alpha], axis=1)
        assert np.all(np.greater(quantiles[1], true_vals))
        assert np.all(np.less(quantiles[0], true_vals))

    def test_linear_regression(self):
        """
        Runs the default fitting algorithm, checks if the resulting posterior
        includes the empirical estimates of the mean and variance in its 99%
        credibility interval.
        """
        prog = """
        mu ~ Normal(5.0, 10.0)
        sigma ~ Gamma(2.0, 10.0)
        y ~ Normal([mu, mu, mu, mu, mu, mu, mu, mu, mu, mu],
                   sigma)
        """

        model = GEM(prog)
        sim = model.sample()
        post, _ = model.fit(
            observed={"y": sim.y},
            n_samples=10000,
            init={"mu": 0.0, "sigma": 0.1},
            num_burnin_steps=2000,
        )

        np.testing.assert_almost_equal(post.mu.mean(), sim.mu, decimal=1)
        np.testing.assert_almost_equal(post.sigma.mean(), sim.sigma, decimal=1)

    def test_simple_sir_mcmc(
        self,
    ):
        """
        Inference on a simple SIR epidemic. beta = 0.00014 gamma = 0.14
        """

        prog = """
        beta ~ Gamma(1.0, 1.0)
        gamma ~ Gamma(1.0, 1.0)
        Epidemic MyEpidemic() {
            S = State(init=999)
            I = State(init=1)
            R = State(init=0)

            [S -> I] = beta * I
            [I -> R] = gamma
        }
        epi ~ MyEpidemic()
        """

        test_dir = os.path.dirname(os.path.realpath(__file__))
        fixture_path = os.path.join(
            test_dir, "..", "fixtures", "fixture_hom_sir_1000.pkl"
        )
        with open(fixture_path, "rb") as f:
            epidata = pkl.load(f)

        model = GEM(prog)
        np.testing.assert_almost_equal(
            model.log_prob(
                beta=epidata["beta"],
                gamma=epidata["gamma"],
                epi=epidata["epi"],
            ),
            -295.0647,
            decimal=3,
        )

    def test_hom_sinr(self):
        """
        Inference on an SINR epidemic model. beta_1 = 0.00015 beta_2 = 0.5
        gamma = 0.14 nu = 0.5 fixture: fixture_hom_sinr_1000.pkl
        """

        prog = """
        beta_1 ~ Gamma(1.0, 1.0)
        beta_2 ~ Gamma(1.0, 1.0)
        gamma ~ Gamma(1.0, 1.0)
        nu ~ Gamma(1.0, 1.0)
        Epidemic SINR() {
            S = State(init=999)
            I = State(init=1)
            N = State(init=0)
            R = State(init=0)

          [S -> I] = beta_1 * I + beta_2 * N
          [I -> N] = gamma
          [N -> R] = nu
        }
        epi ~ SINR()
        """

        test_dir = os.path.dirname(os.path.realpath(__file__))
        fixture_path = os.path.join(
            test_dir, "../fixtures/fixture_hom_sinr_1000.pkl"
        )
        with open(fixture_path, "rb") as f:
            epidata = pkl.load(f)

        model = GEM(prog)
        res = model.log_prob(**epidata)
        np.testing.assert_almost_equal(res, -403.2377, decimal=3)
