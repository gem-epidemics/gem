##############################################################################
# Copyright 2019 Chris Jewell <c.jewell@lancaster.ac.uk>                     #
#                                                                            #
# Permission is hereby granted, free of charge, to any person obtaining a    #
# copy of this software and associated documentation files (the "Software"), #
# to deal in the Software without restriction, including without limitation  #
# the rights to use, copy, modify, merge, publish, distribute, sublicense,   #
# and/or sell copies of the Software, and to permit persons to whom the      #
# Software is furnished to do so, subject to the following conditions:       #
#                                                                            #
# The above copyright notice and this permission notice shall be included    #
# in all copies or substantial portions of the Software.                     #
#                                                                            #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR #
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   #
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    #
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER #
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    #
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        #
# DEALINGS IN THE SOFTWARE.                                                  #
##############################################################################

"""Test the DAG generator"""

import unittest

from gem import GEM
from gem.gemlang.dag import DAG


class TestSimpleDAGGenerator(unittest.TestCase):
    def setUp(self):
        self.expected_graph = "digraph {\n\talpha\n\tbeta\n\tsigma\n\ty\n\t\
alpha -> y\n\tbeta -> y\n\tsigma -> y\n}"
        prog = """
                x = Vector()
                alpha ~ Normal(0, 1000)
                beta ~ Normal(0, 1000)
                sigma ~ Gamma(1, 1)
                y ~ Normal(beta * x + alpha, sigma)
                """
        x = [1.0, 2.0, 3.0, 4.0]
        self.model = GEM(prog, {"x": x})

    def test_dot_generation(self):
        dag = DAG(self.model.ast)
        dot = dag.dot()
        self.assertEqual(dot.source, self.expected_graph)


class TestEpiDAGGenerator(unittest.TestCase):
    def setUp(self):
        self.expected_graph = "digraph {\n\tbeta\n\tgamma\n\tepi\n\tbeta -> epi\n\t\
gamma -> epi\n}"

        prog = """
        beta ~ Gamma(1, 1)
        gamma ~ Gamma(1, 1)

        Epidemic SIR() {
            S = State(init=999)
            I = State(init=1)
            R = State(init=0)

            [S -> I] = beta * I
            [I -> R] = gamma
        }
        epi ~ SIR()
        """
        self.model = GEM(prog)

    def test_dag_generator(self):
        dag = DAG(self.model.ast)
        dot = dag.dot()
        self.assertEqual(dot.source, self.expected_graph)
