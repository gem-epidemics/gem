##############################################################################
# Copyright 2019 Chris Jewell <c.jewell@lancaster.ac.uk>                     #
#                                                                            #
# Permission is hereby granted, free of charge, to any person obtaining a    #
# copy of this software and associated documentation files (the "Software"), #
# to deal in the Software without restriction, including without limitation  #
# the rights to use, copy, modify, merge, publish, distribute, sublicense,   #
# and/or sell copies of the Software, and to permit persons to whom the      #
# Software is furnished to do so, subject to the following conditions:       #
#                                                                            #
# The above copyright notice and this permission notice shall be included    #
# in all copies or substantial portions of the Software.                     #
#                                                                            #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR #
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   #
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    #
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER #
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    #
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        #
# DEALINGS IN THE SOFTWARE.                                                  #
##############################################################################

"""
This test suite examines the GEM grammar and makes sure there is a method in
GEMParser for each production rule specified in the grammar.
"""

import inspect
import os
import re
import unittest

from gem import gemlang
from gem.gemlang.parse_gemlang import ParseTree2AST


# @unittest.skip("TestParseCompleteness to be implemented")
class TestParseCompleteness(unittest.TestCase):
    def setUp(self):

        glpath = os.path.dirname(inspect.getfile(gemlang))
        with open(os.path.join(glpath, "gem_grammar.cfgr"), "r") as gfile:
            self.prod_rules = []
            prod_rule_re = re.compile("[!?]?([a-z][a-z_]*):.*")
            for line in gfile.readlines():
                prod_rule = prod_rule_re.match(line)
                if prod_rule:
                    self.prod_rules.append(prod_rule.groups()[0])

    def test_parse_completeness(self):
        prod_rule_methods = tuple(ParseTree2AST.__dict__)
        for rule in self.prod_rules:
            self.assertIn(
                rule,
                prod_rule_methods,
                f"Production rule '{rule}' not implemented in GEMParser",
            )
