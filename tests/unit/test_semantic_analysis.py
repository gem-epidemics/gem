##############################################################################
# Copyright 2019 Chris Jewell <c.jewell@lancaster.ac.uk>                     #
#                                                                            #
# Permission is hereby granted, free of charge, to any person obtaining a    #
# copy of this software and associated documentation files (the "Software"), #
# to deal in the Software without restriction, including without limitation  #
# the rights to use, copy, modify, merge, publish, distribute, sublicense,   #
# and/or sell copies of the Software, and to permit persons to whom the      #
# Software is furnished to do so, subject to the following conditions:       #
#                                                                            #
# The above copyright notice and this permission notice shall be included    #
# in all copies or substantial portions of the Software.                     #
#                                                                            #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR #
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   #
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    #
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER #
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    #
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        #
# DEALINGS IN THE SOFTWARE.                                                  #
##############################################################################

"""Testing semantic analysis"""

import unittest

from gem.gemlang import GEMParser
from gem.gemlang.semantics.symbol import VariableSymbol
from gem.gemlang.semantics.symbol_declare import SymbolDeclarer
from gem.gemlang.semantics.symbol_resolve import SymbolResolver
from gem.gemlang.semantics.symbol_table import SymbolTable


class TestParseDeclarations(unittest.TestCase):
    """Test declaration of symbols, and simple resolution"""

    def test_decl_assign(self):
        """Simple assign"""

        prog = """
        pi = 3.14157
        """
        ast = GEMParser.__call__(prog)
        symtab = SymbolTable()
        SymbolDeclarer().visit(ast, symtab)
        assign = ast.children[0]
        pi_ref = assign.children[0]
        self.assertTrue(symtab.globals.resolve("pi"))
        self.assertIsInstance(symtab.globals.resolve("pi"), VariableSymbol)
        self.assertIs(pi_ref.symbol, symtab.globals.resolve("pi"))
        self.assertIs(symtab.globals.resolve("pi").ast_def, pi_ref)

    def test_decl_stochastic(self):
        """Declare a stochastic variable"""

        prog = """
        rv ~ Gamma(0.1, 0.1)
        """
        ast = GEMParser.__call__(prog)
        symtab = SymbolTable()
        SymbolDeclarer().visit(ast, symtab)
        assign = ast.children[0]
        rv_ref = assign.children[0]
        self.assertTrue(symtab.globals.resolve("rv"))
        self.assertIsInstance(symtab.globals.resolve("rv"), VariableSymbol)
        self.assertIs(rv_ref.symbol, symtab.globals.resolve("rv"))
        self.assertIs(symtab.globals.resolve("rv").ast_def, rv_ref)


class TestResolveSymbols(unittest.TestCase):
    """Basic symbol resolution"""

    def test_resolve_assign(self):
        prog = """
        pi = 3.14157
        pi * 2
        """
        ast = GEMParser.__call__(prog)
        symtab = SymbolTable()
        SymbolDeclarer().visit(ast, symtab)
        SymbolResolver().visit(ast, symtab)
        mul_expr = ast.children[1]
        pi_ref = mul_expr.children[0]
        self.assertIs(symtab.globals.resolve("pi"), pi_ref.symbol)

    def test_resolve_stochastic_assign(self):
        """Can we resolve gamma on line 2?"""

        prog = """
        gamma ~ Gamma(0.1, 0.1)
        gamma * 2
        """
        ast = GEMParser.__call__(prog)
        symtab = SymbolTable()
        declarer = SymbolDeclarer()
        declarer.visit(ast, symtab)
        SymbolResolver().visit(ast, symtab)
        mul_expr = ast.children[1]
        gamma_ref = mul_expr.children[0]
        self.assertIs(symtab.globals.resolve("gamma"), gamma_ref.symbol)
