##############################################################################
# Copyright 2019 Chris Jewell <c.jewell@lancaster.ac.uk>                     #
#                                                                            #
# Permission is hereby granted, free of charge, to any person obtaining a    #
# copy of this software and associated documentation files (the "Software"), #
# to deal in the Software without restriction, including without limitation  #
# the rights to use, copy, modify, merge, publish, distribute, sublicense,   #
# and/or sell copies of the Software, and to permit persons to whom the      #
# Software is furnished to do so, subject to the following conditions:       #
#                                                                            #
# The above copyright notice and this permission notice shall be included    #
# in all copies or substantial portions of the Software.                     #
#                                                                            #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR #
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   #
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    #
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER #
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    #
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        #
# DEALINGS IN THE SOFTWARE.                                                  #
##############################################################################

"""Test symbol functionality"""

import unittest

from gem.gemlang.semantics import symbol as symlib
from gem.gemlang.semantics.scope import GlobalScope, LocalScope
from gem.gemlang.semantics.symbol_table import SymbolTable


class TestScope(unittest.TestCase):
    def test_global_scope(self):
        g = GlobalScope()

        self.assertIs(g.name, "global")

        # Resolve a builtin
        self.assertIs(g.enclosing_scope, None)

        # Declare and resolve
        g.declare(symlib.VariableSymbol("foo"))
        self.assertIsInstance(g.resolve("foo"), symlib.VariableSymbol)

    def test_local_scope(self):
        g = GlobalScope()
        local_scope = LocalScope(enclosing_scope=g)
        local_scope.declare(symlib.VariableSymbol("foo"))
        self.assertIsInstance(
            local_scope.resolve("foo"), symlib.VariableSymbol
        )


class TestScopedSymbol(unittest.TestCase):
    def test_method_symbol(self):
        g = GlobalScope()
        g.declare(symlib.MethodSymbol("Foo", "Scalar", g))

        m = g.resolve("Foo")
        m.declare(symlib.VariableSymbol("bar", "Scalar"))
        m.declare(symlib.VariableSymbol("baz", "Scalar"))

        # Tests ability to resolve up the scope
        local = LocalScope(enclosing_scope=m)
        self.assertIsInstance(local.resolve("bar"), symlib.VariableSymbol)


class TestSymbolTable(unittest.TestCase):
    def test_symbol_table(self):  # pylint: disable=no-self-use
        """Can we instantiate a symbol table?"""
        SymbolTable()

    def test_symbol_sir(self):  # pylint: disable=no-self-use
        """Make sure parse works without errors"""

        from gem import GEM

        prog = """
        beta ~ Gamma(1.0, 1.0)
        gamma ~ Gamma(1.0, 1.0)
        Epidemic SIR() {
            S = State(init=999)
            I = State(init=1)
            R = State(init=0)

            [S -> I] = beta * I
            [I -> R] = gamma
        }
        epi ~ SIR()
        """
        GEM(prog)
